.. image:: https://etnadji.fr/pyscribus/_static/logo-300.png
    :alt: PyScribus logo
    :align: center

A library aiming to read, generate and update Scribus documents (SLA).

----

.. image:: https://nogithub.codeberg.page/badge.svg
    :alt: Please don't upload to GitHub
    :target: https://nogithub.codeberg.page
.. image:: https://img.shields.io/pypi/v/pyscribus.svg?label=latest%20version
    :alt: PyPi Latest Version
    :target: https://pypi.python.org/pypi/pyscribus
.. image:: https://www.repostatus.org/badges/latest/wip.svg
    :alt: Project Status: WIP – Initial development is in progress, but there has not yet been a stable, usable release suitable for the public.
    :target: https://www.repostatus.org/#wip

----

For Python versions older than 3.8, see 
`pyscribus-backported <https://pypi.org/project/pyscribus-backported>`_ package.

- `Quickstart <https://etnadji.fr/pyscribus/guide/en/quickstart.html>`_


