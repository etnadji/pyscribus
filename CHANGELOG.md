# Changelog

Dates are in DD/MM/YYYY format.

## 0.3 -> 0.3.x (not yet released)

This version will introduce some interesting things for people intending to
add frames and manipulate text with PyScribus.

It pythonize the creation of text, image, polygon frames; it simplifies the
creation/manipulation of stories; it allow programmers to use their own unit 
(millimeters, inches) instead of raw picas points values.

### Model

#### Common/Maths

Introduced classes and functions for unit conversion/abstraction, so that 
the programmer can use it's own unit, when the model is still given picas
points values.

See commits b0f3ed91,

#### Page objects

See commits 891466db, 545222db.

- Generalising page objects attributes to any page object class.
- Handling image objects effects (leaving their parameter as-is)

#### Document

See commits 834ed9a4, 9fe0c0aa, 

- Handling PDF (``//PDF``) color profiles settings: ``@SolidP``, ``@ImageP``,  
  ``@PrintP``, ``@UseProfiles``, ``@UseProfiles2``, ``@Intent``, ``@Intent2``.

#### Fixes

- 8ad7c122: Fix the export of attributes related to document border margins,
  text tool, calligraphic pen tool.
- d4b440fa: Removed unnecessary attributes of character styles when they have
  a parent character style defined.
- 59015e19: Handling non rectangular and non closed page objects paths
- b4e8a757: Regular polygons (with ``@PTYPE="13"``) were not exported.

### File

#### UI interface

See commits 2d3b2d24, 2bd7851e,

- Added interface for the “Line” tool.
- Added properties for UI snapping to guides, grids, objects.

#### Pages interface

See commits a0a35d54,

- Added interfaces to individual (master)pages.

#### Page objects interfaces

See commits 97f46594, 4f1d55f7

- PageObject interface:
  - Creating text object with ``new_text`` method.
  - Creating image object with ``new_image`` method.
  - Creating polygon object with ``new_polygon`` method.

#### Color interfaces

See commits 153697e3,

- Added an interface to individual colors.

#### Stories

See commits ff1871d1, 37cd8f10, 206d6d54,

- Made interfaces ``Story``, ``Paragraph``, ``SpanText``, ``Text`` usable.
- Added interfaces to spaces (``Space``), breaks (``Break``), unicode 
  joiners(``Unicode``).

## 0.2.5 -> 0.3 “Don't trust the shepherd” (04/08/2023)

This version breaks compatibility with previous versions, as it splits
code between a SLA **model** (mostly the code of [0.2.5](#024-025-22072023)) and a
(pythonified) interface subpackage called **file**.

[Reading the documentation](https://etnadji.fr/pyscribus/) is highly recommended.

Aside from that, this version fixes an unpleasant, important regression 
introduced in the previous version (0.2.5) making text frames' content 
invisible. Regarding the model, changes are mostly/notably:

1. Handling new ``//DOCUMENT`` attributes linked to UI settings. On 
   174 attributes, when version 0.2.5 handled 41, version 0.3 handles 98.
2. Re-arranging export order of ``//DOCUMENT`` attributes. It is a step towards 
   fidelity to Scribus output. It also make diff easier ;-)
3. Fixing bugs in attributes exports.

See below for more details.

From now, in the changelog sections “Model” and “File”, respectively the 
`pyscribus.model` and `pyscribus.file` modules will be omitted when referring 
to code in those same modules.

### Model

#### Document

See commits bf81bd46, 882a8126, 44283c7a, 0dc8d8bf, 8dd1f994, ca00878d, 
34e7bedd, 3267ffc7.

- New order of exporting the attributes of ``//DOCUMENT``, towards
  fidelity to Scribus output (882a8126, 44283c7a).
- Handling UI show/hide of frames (``@SHOWFRAME``), invisible/control text
  caracters (``@SHOWControl``), layer hints (``@SHOWLAYERM``), document bleeds 
  (``@showBleed``).
- New handling of some document-wide settings:
  - Unit setting (``@UNITS``).
  - Scratch space: ``@ScratchBottom``, ``@ScratchLeft``, ``@ScratchRight``, 
    ``@ScratchTop``, ``@GapHorizontal``, @GapVertical``.
  - Tools defaults:
    - Line tool: ``@StartArrow``, ``@EndArrow``, ``@PENLINE``, ``@STILLINE``,
      ``@WIDTHLINE``, ``@LINESHADE``.
    - Polygon tool: ``@POLYF``, ``@POLYC``, ``@POYLR``, ``@POLYIR``,
      ``@POLYCUR``, ``@POLYOCUR``, ``@POLY``.
    - Shape tool: ``@PEN``, ``@BRUSH``, ``@STIL``, ``@WIDTH``, ``@PENSHADE``,
      ``@BRUSHSHADE``.
    - Text tool: font (``@DFONT``), font size (``@DSIZE``), column number for 
      text frames (``@DCOL``), size of gaps between text frames columns 
      (``@DGAP``).
  - Grids settings:
    - Minor and major grids: ``@MINGRID``, ``@MAJGRID``, ``@MINORC``,
      ``@MAJORC``.
    - Baseline: ``@BASEO``, ``@BASEGRID``, ``@BaseC``.
  - Handling hyphenation (``//HYPHEN`` and its children).

#### Fixes

- 5e946a0d: Image page objects not being loaded.
- 1e9c6550: Texts being invisible due to bad opacity parameter export 
  (regression introduced in 0.2.5).
- 34e7bedd: Frames not showing at the right coordinates because document's 
  scratch space weren't handled.
- 18b967a9: ``colors.Colors.set_colors()`` failing on property `space`.
- 8264bedd: Stories not inheriting their parent paragraph style.
- 4aeef277: Stories fragment and resets not exporting their parent character 
  style.
- acd1c5c3: Fixed export of ``@BGCOLOR``, ``@BCOLOR``, ``@BGSHADE``, ``@BSHADE``
  attributes of ``//STYLE`` and `//CHARSTYLE`.
- 11ff6dee: Fixed ``@OpticalMargins`` and ``@BulletStr`` attributes of 
  ``//STYLE`` and `//CHARSTYLE` being exported when it was not necessary.
- c85b5254: Export of values in seconds units.
- c188c9b9: Useless decimals in exporting `//STYLE/@FONTSIZE` and 
  `//CHARSTYLE/@FONTSIZE`.
- 6b5753bc: Useless decimals in exporting ``@AGverticalAutoGap`` and
  ``@AGverticalAutoGap`` attributes of ``//MASTERPAGE`` and ``//PAGE``.
- 6b5753bc: Useless decimals in exporting ``//PAGEOBJECT/@gWidth`` and 
  ``//PAGEOBJECT/@gHeight``.
- 3a8bd17c:
  - Useless decimals in exporting ``//PAGEOBJECT/@LOCALSCX``, 
    ``//PAGEOBJECT/@LOCALSCY``, ``//PAGEOBJECT/@PWIDTH``, ``//PAGEOBJECT/@COLGAP``.
  - Useless decimals in exporting ``//PDF/@BleedTop``, ``//PDF/@BleedBottom``, 
    ``//PDF/@BleedLeft``, ``//PDF/@BleedRight``.
  - ``styles.StyleAbstract.font["size"]`` is now a ``Dim`` object. 
    See also c188c9b9.
  - Useless decimals in exporting ``//STYLE/@LINESP``, ``//STYLE/@INDENT``, 
    ``//STYLE/@RMARGIN``, ``/*STYLE/@FIRST``, ``//CHARSTYLE/@SSHADE``.
- 3a6371c7: Cosmetic fix of exporting character styles and paragraph styles.

### File

First version with this submodule, so… everything?

Interfaces implemented in this module are accessible through ``ScribusFile`` 
class properties.

| ScribusFile property | Related file submodule & class | Related model submodule & class |
| -------------------- | ------------------------------ | ------------------------------- |
| ``colors`` | ``colors.Colors`` | ``colors`` |
| ``hyphenation`` | ``hyphenation.Hyphenation`` | ``document.Hyphenation`` |
| ``metadatas`` | ``document.Metadatas`` | ``document.Document`` |
| ``pageobjects`` | ``pageobjects.PageObjects`` | ``document.Document``, ``pageobjects`` |
| ``pages`` | ``document.Pages`` | ``document.Document``, ``pages`` |
| ``stories`` | ``stories.Stories`` | ``document.Document``, ``stories`` |
| ``templatable_stories`` | ``stories.Stories`` | ``document.Document``, ``stories`` |
| ``ui`` | ``ui.UI`` | ``document.Document`` |
| ``ui.tools`` | ``ui.Tools`` | ``document.Document`` |

## 0.2.4 -> 0.2.5 (22/07/2023)

### Known regression(s) fixed in later versions

- **Fixed in 0.3**. Object fill opacity (`@TransValue`) is exported when it is not 
  necessary, leading to invisible texts. If your texts objects do not use 
  default opacity settings, remove ``@TransValue`` from your files to fix that.

### SLA

- Fixed `SLA.stories()` when it asks for all existing stories.

### Document

See commits 65132615, a6878fc, 899be579.

- Added the method `style()` to return one or more existing styles.
- Handling during load some attributes (`@ANZPAGES`, `@BORDERLEFT`, 
  `@BORDERRIGHT`, `@BORDERTOP`, `@BORDERBOTTM`, `@KEYWORDS`, `@PAGEHEIGHT`, 
  `@PAGEWIDTH`) that were handled only during export.

#### CheckProfile

See commit 304ee181.

- During export, align attribute order to Scribus export order.

### Page objects

#### Multiple page object types

See commits 93cd2e60, bc10aaff.

- Text flow mode for polygon, polyline and text objects (`@TEXTFLOWMODE`).
- Object fill opacity (`@TransValue`).

#### Texts

##### Text-on-path

See commits 7c9388ee, 98eeb71, 7b0fbd2.

- Show the path (`@PLTSHOW`).
- Offset of the path (`@BASEOF`).
- Export story.

#### Tables

See commits 0f8cda21.

- Table style (`PAGEOBJECT/@TableStyle`). Older attribute `TableObject.style` 
  becames `TableObject.data_style` as it is for table data, not table itself.

#### Images

See commits 0f8cda21, 016ee58c.

- Rendering intent for images (`@IRENDER`).
- Inline image marker (`@isInlineImage`).
- Page number (`@Pagenumber`).
- Compression method (`@COMPRESSIONMETHOD` or `@CMethod`).

### Styles

#### Paragraph

See commits 8a130601, 0bf0c823, b4e38299.

Fixed:

- Setting defaults values.
- Export of optical margins (`@OpticalMargins`) and leading (`LINESP`) settings.

New handling:

- Ordered lists numeration types.
- Hyphenation consecutive lines (`@HyphenConsecutiveLines`).
- Kerning (`@KERN`).
- Leading (`LINESP`) in automatic mode.

#### Cell

See commit 11a78bc2.

- During export, align attribute order to Scribus export order.

### Printing

See commit 5bfebaca.

#### PDF settings

Fixed:

- LPI settings not being exported.

New handling:

- Use LPI (`@UseLpi`)
- PDF version (`@Version`)
- Generating/Embedding objects (`@EmbedPDF`, `@Articles`, `@Bookmarks`,
  `@Thumbnails`)
- Transformation settings (`@MirrorH`, `@MirrorV`, `@RotateDeg`)

#### LPI

Fixed:

- Some attributes export (`@Frequency`, `@Angle`)

## 0.2.3 -> 0.2.4 “Death to article 49.3” (16/03/2023)

### Headless

Implemented in 316ac79.

- Use `pyscribus.headless.run_pyh_script` to execute Scribus headless scripts.
- `topdf` script convert SLA files to PDF.

### Dimensions

- `Dim.__init__` use kwargs instead of arguments, except for value.
- Negative values in Dim using pica unit can be allowed, fixing #1.

### Document

Better / new handling of :

- `@ORIENTATION` export is fixed

See commit 641c1332.

### Pages

Better / new handling of :

- Auto guides selection (`@AGSelection`)
- Leftest page `@LEFT` attribute

See commit b0483718.

### Styles

Most of the implementations discussed here happened in commit 87335b37, b3167c6.

#### Paragraph

Better / new handling of :

- Background color & shade (`@BCOLOR`, `@BSHADE`)
- Text direction (`@DIRECTION`)
- Optical margins (`@OpticalMargins`)
- Glyph scaling (`@MinGlyphShrink`, `@MaxGlyphExtend`)
- Initial/Drop capital (`@ParagraphEffectIndent`, `@ParagraphEffectOffset`, `@DROPDIST`)
- Allow negative value in first-line indent (`@FIRST`)

#### Character

- Background color & shade (`@BGCOLOR`, `@BGSHADE`)
- Stroke color & shade (`@SCOLOR`, `@SSHADE`)
- Scaling base line offset (`@BASEO`)

## 0.2.2 -> 0.2.3 (11/03/2023)

First release with better SLA handling in a while. Big improvement in stories 
templating and document page settings.

### Templating (SLA)

In-text pattern and attribute pattern are the same.

Allow consecutive data if multiple occurrences of templating markers.

Most of the implementations discussed here happened in commits 71fe7157, ddd4dcca.

### Document

Better / new handling of :

- Automatic textes frames (`@AUTOSPALTEN`, `@ABSTSPALTEN`)
- Page settings in DOCUMENT (`@ORIENTATION`, `@PAGESIZE`, `@BOOK`)

Most of the implementations discussed here happened in commits ddd4dcca.

## 0.2.1 -> 0.2.2 (06/03/2023)

A version made of bug fixes.

### Page objects

Fixed PageObject being wrongly filled by black color.

See commit bd539dc0.

### Printing

Fixed (for now) the export of PDF bleeds.

See commit 808ce211.

### Templating (SLA and Stories)

Fixed templating (correct in-text pattern, and actual replacement).

See commit a92c7643.

## 0.2 -> 0.2.1 (21/11/2020)

### Package

Correct `__version__` as it still was "0.1" in "0.2"…

## 0.1 -> 0.2 (21/11/2020)

### Package and dependencies

- New dependency : svg.path.
- Link to issues webpage
- New package for python versions < 3.8 : pyscribus-backported

### Pages / Master pages

- Handling of PDF effects (`@pageEffectDuration`, `@pageViewDuration`, `@effectType`, `@Dm`, `@M`, `@Di`) => `PageAbstract.effect`
- Actual export of guides and autoguides

Most of the implementations discussed here happened in commit f86ddef4.

### Page objects

Better / new handling of :

- outline (`@PWIDTH`, `@PLINEART`, `@PCOLOR`, `@PCOLOR2`, `@PLINEEND`, `@PLINEJOIN`) => `PageObject.outline`.
- image frames in image objects (`@EMBEDDED`, `@RATIO`, `@SCALETYPE`, `@LOCALSCX`, `@LOCALSCY`, `@LOCALX`, `@LOCALY`, `@LOCALROT`, `@PICART`) => `PageObject.use_embedded_icc`, `PageObject.image_scale`, `PageObject.image_box`, `PageObject.image_rotated_box`
- group objects (`@groupWidth`, `@groupHeight`) => `GroupObject.group_box`

Handling of `@gXpos`, `@gYpos`, `@gWidth`, `@gHeight`, even if I don’t know the utility of it => `PageObject.gbox`

Most of the implementations discussed here happened in commits a35231bc, 85695910.

### Styles

#### Paragraph

- Indentations (`@INDENT`, `@RMARGIN`, `@first`) => `ParagraphStyle.indentations`.
- Ordered and bullet lists settings (`@Numeration`, `@Bullet`, `@BulletStr`) => `ParagraphStyle.listing`.

Most of the implementations discussed here happened in commits b0702c1e, 8706565e,85695910.

#### Character

- Space width (`@wordTrack`) => `CharacterStyle.font`.
- Kerning (`@KERN`) => `CharacterStyle.font`.

Most of the implementations discussed here happened in commit b0702c1e.

### Extra

#### Wireframe

- Fixed `Wireframe.from_sla()` as `sla.SLA.documents` is now `sla.SLA.document` and not a list anymore.

## 0.1 (26/05/2020)

First version.

<!-- vim:set spl=en: -->
