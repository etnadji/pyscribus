.PHONY: help Makefile clean clean-tests reports clean-reports reporttodo reportfix checkver

# You can set these variables from the command line.
SPHINXOPTS    =
SPHINXBUILD   = sphinx-build
SOURCEDIR     = source
BUILDDIR      = build

# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%: Makefile
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

clean: clean-tests
	-rm source/*.pyc
	-rm source/__pycache__/*.pyc
	-rmdir source/__pycache__
	-rm source/pyscribus/*.pyc
	-rm source/pyscribus/__pycache__/*.pyc
	-rmdir source/pyscribus/__pycache__
	-rm source/pyscribus/common/*.pyc
	-rm source/pyscribus/common/__pycache__/*.pyc
	-rmdir source/pyscribus/common/__pycache__
	-rm source/pyscribus/papers/*.pyc
	-rm source/pyscribus/papers/__pycache__/*.pyc
	-rmdir source/pyscribus/papers/__pycache__

clean-tests:
	-rm source/tests-outputs/*

reports: reporttodo reportfix

clean-reports:
	-rm thingstodo.txt
	-rm thingstofix.txt

reporttodo:
	grep -n "TODO" source/pyscribus/*.py > things_to_do.txt

reportfix:
	grep -n "FIXME" source/pyscribus/*.py > things_to_fix.txt

sphinxdoc:
	sphinx-build -b html source/ build/

pipinstall-testing:
	pip3 install --index-url https://test.pypi.org/simple/ --no-deps pyscribus

pipinstall:
	pip3 install pyscribus

pipremove:
	pip3 uninstall pyscribus

package: checkver
	./make-pypi.sh

pypupload-testing:
	./upload-pypi-testing.sh

pypupload:
	./upload-pypi-main.sh

checkver:
	@echo "package :" `grep "__version__" source/pyscribus/__init__.py`
	@echo "setup.py :" `grep "VERSION" source/setup.py | head -1`

formatfile:
	black -l 79 source/pyscribus/file/*.py

formatmodel:
	black -l 79 source/pyscribus/model/*.py
	black -l 79 source/pyscribus/model/common/*.py
	black -l 79 source/pyscribus/model/extra/*.py
	black -l 79 source/pyscribus/model/headless/*.py
	black -l 79 source/pyscribus/model/headless/scripts/*.py
	black -l 79 source/pyscribus/model/papers/*.py
