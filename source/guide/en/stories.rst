*************************
Working with text/stories
*************************

Scribus and PyScribus stories' handling
=======================================

Scribus
-------

Scribus handles stories' content as a sequence of elements. Following text 
contents that use the same formatting rules are joined in one text fragment. 
Unlike HTML paragraphs (``<p>`` elements), Scribus paragraphs are not enclosed 
in one element, but are text fragments followed by a end-of-paragraph mark.

For example, the paragraph, composed in Arial and encoded in HTML:

  ::

   <p>I <em>like</em> cake.</p>

Can be encoded like this in Scribus format:

.. code:: xml

   <ITEXT CH="I " />
   <ITEXT FONT="Arial Italic" CH="like" />
   <ITEXT CH=" cake." />
   <para/>

PyScribus
---------

*file* submodule of PyScribus handles stories' content as a sequence of
paragraphs, containing a sequence of text fragments, breaks, etc, like 
russian nesting dolls:

  ::

   | Story
     | Paragraph
       | Text or
       | SpanText
         | Python string or
         | Field or
         | Break or
         | Space or
         | Unicode (non)joiners


Handling a text frame's story
=============================

A text frame's story can be accessed and set with the ``story`` property.

Creating a new story
====================

If you'd like to work on the contents of a story before assigning it to a text
frame, here's how an ``Story`` instance is created:

  ::

   story = document.stories.new_story()

Adding paragraphs and text content
==================================

Except classes for special characters (see below), most *file* classes for
stories management have a ``append`` method:

  ::

   # Will be omitted in the next examples of this section
   from pyscribus.file.stories import Text, SpanText
   story = document.stories.new_story()

   para = story.new_paragraph(style="ColoredRed")
   para.append(Text(text="Aaaaargh"))
   story.append(para)

Paragraph ``append`` method accepts instances or list of instances. See their
docstrings for the list of accepted classes.

If you want to set the content at instanciation, use the ``sequence``
parameter:

  ::

   # Equivalent to the previous example. sequence argument takes a list.
   story.append(
       story.new_paragraph(
           sequence=[Text(text="The Grail is in the castle of…")]
       )
   )

Paragraphs can use a specific paragraph style, accessed with the ``style``
property.

Text fragments
--------------

Text fragments can use a specific character style, accessed with the
``character_style`` property.

Append ``Text`` instances for text fragments without any additional (i.e.
differing from paragraph formatting settings) formatting.

Append ``SpanText`` instances for text fragments with additional formatting.

  ::

   story.append(
       story.new_paragraph(
           sequence=[
               Text(text="The "),
               SpanText(text="Grail", character_style="Underlined"),
               Text(text=" is in the castle of…"),
           ]
       )
   )

   story.append(
       story.new_paragraph(style="ColoredRed", sequence=[Text(text="Aaaaargh")])
   )

Adding special characters
=========================

Most special characters can be inserted like ordinary Unicode characters, with 
the exception of the non-breaking hyphen and the non-breaking space.

.. note::

   Those exceptions are due to Scribus's handling of these characters.

PyScribus offers classes for inserting certain of those special characters.
Special characters can only be appended to text fragments:

  ::

   story.append(
       story.new_paragraph(
          sequence=[
              Text(text="In french, we insert non-breaking spaces before ")
              Text(text="colons, and before/after or some quotation marks:")
          ]
       )
   )

   # Regular text content is just strings in Text or SpanText sequences.

   sentence = Text(
      sequence=[
          "«",
          Space(type="nbsp"),
          "On dit",
          Space(type="nbsp"),
          ": pain au chocolat.",
          Space(type="nbsp"),
          "»",
      ]
   )

   # « On dit : pain au chocolat. »
   # If a baker from the south of France attacks me, that might be the reason…


Adding breaks
=============

You can insert line, frame, columns breaks with ``Break`` instances.

  ::

   para = story.new_paragraph()
   para.append(Text(text="A"))
   para.append(Break(type="line"))

   para.append(Text(text="B"))
   para.append(Break(type="column"))

   para.append(Text(text="C"))
   para.append(Break(type="frame"))

   para.append(Text(text="D"))

Adding fields
=============

You can insert total page count, current page number fields with ``Field``
instances.

  ::

   para = story.new_paragraph()
   para.append(Field(type="page"))
   para.append(Text(text=" / "))
   para.append(Field(type="pages"))

