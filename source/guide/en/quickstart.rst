**********
Quickstart
**********

Install PyScribus
=================

Pip
---

PyScribus is `available on PyPI <https://pypi.org/project/pyscribus/>`_ :

**For Python versions >= 3.8 :**

  ::

   pip install pyscribus

**For older Python versions :**

  ::

   pip install pyscribus-backported

Git
---

The code of PyScribus is 
`available as a GIT repository on Framagit <https://framagit.org/etnadji/pyscribus>`_.

  ::

   git clone https://framagit.org/etnadji/pyscribus.git

.. _model_file_organisation:

PyScribus' low and high level submodules
========================================

.. warning::

   This is introduced in PyScribus 0.3 and breaks compatibility.

PyScribus is organized into two main submodules: ``model`` and ``file``.

- ``model`` seeks to transpose the SLA format into Python objects. It aims to 
  load and export an SLA file correctly, but not to modify its content by 
  itself.
- ``file`` is an interface to ``model``. Its purpose is to enable pythonic 
  manipulation and modification of an SLA file. As such, it is **the preferable 
  way to use PyScribus**.

``file`` provide interfaces for parts of the Scribus document, like pages
page objects, styles…

.. note::

   Depending on when you read this documentation, some of those interfaces 
   may or may not exist/be complete.

Accessing the model of a file
-----------------------------

If you now what you are doing, you can access the Scribus model wrapped by 
``ScribusFile`` using its ``model`` attribute.

  ::

   import pyscribus.file as psf

   parsed = psf.ScribusFile("1.5.5", "intro.sla")

   # pyscribus.file offers a `pages` attribute to do that with 
   #    `len(parsed.pages)`
   # But you can do it like this if you really want to:

   print(len(parsed.model.document.pages))


Frequent tasks
==============

Reading an existing SLA file
----------------------------

  ::

   import pyscribus.file as psf

   # example.sla is parsed at instanciation
   parsed = psf.ScribusFile("1.5.5", "example.sla")

Generating an SLA file
----------------------

  ::

   import pyscribus.file as psf

   generated = psf.ScribusFile("1.5.5")

   # Loading default content and settings
   generated.fromdefault()

   # Here you can do some modifications
   # Then:

   # Save the file
   generated.save("generated.sla")

.. Adding things to a SLA file
.. ---------------------------
.. You can access SLA datas through object attributes, but most of the time, if you 
.. want to append something (document, pages, master pages, layers, colors, page 
.. objects and styles), use the ``SLA.append()`` function.

Defining your units
-------------------

Some numerical values in Scribus are expressed in pica/DTP points, a unit 
you're probably not comfortable working with. 

Objects in Scribus “float” in a two-dimensional plane (called scratchspace) 
that can have margins. Since these margins must be taken into account when 
converting coordinates to picas points, PyScribus offers functions for 
converting values in millimeters, inches, etc, into pica points. 

  ::

   import pyscribus.model.common.math as maths

   # 5 millimeters to picas
   gap_size = maths.mm(5)
   # 5 inches to picas
   gap_size = maths.inch(5)

   # Coordinates.
   # You have to know your scratchspace's margins for those.

   # 14.111 mm, 100 picas of scratchspace margin in X axis -> 140 picas
   mm_x = maths.to_pica_coord(14.111, "mm", 100)
   # Reverse operation -> 14.111 mm
   picas_x = maths.to_unit_coord(140, "mm", 100)


To abstract these conversions, set the argument ``coding_unit`` of 
``ScribusFile``:

  ::

   import pyscribus.file as psf

   # example.sla is parsed at instanciation
   parsed = psf.ScribusFile("1.5.5", "example.sla", coding_unit="mm")

You will then be able to give millimeters, inches, etc, to values requiring 
picas.

.. note::

   This applies to values in picas accessible through a class present in the 
   submodule ``pyscribus.file``. The model classes still expect picas.

  ::

   import pyscribus.file as psf

   # example.sla is parsed at instanciation
   parsed = psf.ScribusFile("1.5.5", "example.sla", coding_unit="mm")

   text_frame = parsed.pageobjects.new_text(
       # 40 picas in X,Y (abstracting the scratchspace margins) -> 14.111 mm
       pos_x=14.111,
       pos_y=14.111,
       # Around 283 picas in width, height -> 100 mm
       width=100,
       height=100,
   )


See :py:meth:`pyscribus.ScribusFile.set_coding_unit` for a complete list of
valid values.

Filtering and iterating over page objects
-----------------------------------------

  ::

   import pyscribus.file as psf

   parsed = psf.ScribusFile("1.5.5", "example.sla")

   # Not necessary, just to avoid writing parsed.pageobjects over and over
   doc_objects = parsed.pageobjects

   # You can iterate over images_frames, but this example do not.
   image_frames = doc_object.filter(object_type="image")

   # Iterating over all page objects
   for page_object in doc_objects:
       print(page_object.name)

Adding page objects
-------------------

Code:

  ::

   import pyscribus.file as psf

   # Model module for stories
   import pyscribus.model.stories as m_stories

   # Parse example.sla
   parsed = psf.ScribusFile("1.5.5", "example.sla")

   # Not necessary, just to avoid writing parsed.pageobjects over and over
   doc_objects = parsed.pageobjects

   # Create a text frame containing a story ----------

   text_frame = doc_objects.new_text(
       # Values are inserted in picas, here.
       # Those arguments' names match text_frame properties
       pos_x=140,
       pos_y=60,
       width=100,
       height=100,
   )

   # Adds an original text to the story using the model classes.

   text_frame.object.stories[0].append(
       m_stories.StoryFragment(text="Hello world!")
   )

   # -------------------------------------------------

   # Append the frame through pageobjects interface
   doc_objects.append(text_frame)

   # Save the file
   parsed.save("modified.sla")

