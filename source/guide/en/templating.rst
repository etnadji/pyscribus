Templating
==========

What you can do with PyScribus templating
-----------------------------------------

- Find appropriate page object with page object attributes
- Inject text into text frames stories

Activation & configuration
--------------------------

To activate templating features of PyScribus, first add the parameter 
``templating`` to the SLA object (pyscribus.sla.SLA) :

  ::

   import pyscribus.file as pyf

   # intro.sla is parsed at instanciation
   parsed = pyf.ScribusFile("1.5.5", "template.sla", templating=True)

You can set some other templating parameters:

+-----------------------+---------------------------+---------------+
| Kwarg key             | Use                       | Default value |
+=======================+===========================+===============+
| templatingInsensitive | Should in-text templating | ``False``     |
|                       | be case insensitive ?     |               |
+-----------------------+---------------------------+---------------+
| templatingPattern     | compiled regex to find    | ``(%\w+%)+``  |
|                       | templated elements        |               |
|                       |                           |               |
|                       | (ex: %TITLE%)             |               |
+-----------------------+---------------------------+---------------+

Load data
---------

  ::

   # Getting all stories with template-able content
   stories = parsed.templatable_stories

   # Making our template datas for stories

   # If the value of datas[key] is a string, each occurrence of that key will
   # be filled by datas[key] value. In this example, %Title% and %Text% values
   # are always the same.

   # If the value of datas[key] is a list of strings, each occurence of that
   # key will be replace by the value at datas[key][index]. If there is more
   # placeholders than there is values, those placeholders remains. In this 
   # example, %Item% placeholders will be replaced by "Item 1", then "Item 2".
   # If there is more %Item% placeholders, they will not be replaced.

   datas = [
       {
           "%Title%": "My title",
           "%Text%": "Lorem ipsum",
           "%Item%": ["Item 1", "Item 2"]
       }
   ]

   # For each stories, we replace/feed the placeholders with their contents

   for index, templatable_story in enumerate(stories):
       parsed.stories.feed(templatable_story, datas[index])

   # Saving the templated document as a new one

   template.save("templated.sla")
