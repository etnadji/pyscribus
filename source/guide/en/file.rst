**************
PyScribus file
**************

The subpackage ``file`` acts as an interface between the programmer and the 
SLA file format implemented in the submodule ``model``.

The manipulation of a SLA file with ``file`` is done with the ``ScribusFile`` 
methods and properties. Each class docstring related to a ``ScribusFile`` 
property contains a small example of it's usage.

+-------------------------+-----------------+------------------+
| Property                | File submodule  | Model submodule  |
+=========================+=================+==================+
| ``colors``              | ``colors``      | ``colors``       |
+-------------------------+-----------------+------------------+
| ``hyphenation``         | ``hyphenation`` | ``document``     |
+-------------------------+-----------------+------------------+
| ``metadatas``           | ``document``    | ``document``     |
+-------------------------+-----------------+------------------+
| ``pageobjects``         | ``pageobjects`` | ``pageobjects``  |
+-------------------------+-----------------+------------------+
| ``pages``               | ``document``    |  ``pages``       |
+-------------------------+-----------------+------------------+
| ``stories``             | ``stories``     |  ``stories``     |
+-------------------------+-----------------+------------------+
| ``templatable_stories`` | ``stories``     |  ``stories``     |
+-------------------------+-----------------+------------------+
| ``ui``                  | ``ui``          | ``document``     |
+-------------------------+-----------------+------------------+
| ``ui.tools``            | ``ui``          | ``document``     |
+-------------------------+-----------------+------------------+

