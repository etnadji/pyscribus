#!/usr/bin/python3
# -*- coding:Utf-8 -*-

# PyScribus, python library for Scribus SLA
# Copyright (C) 2020-2023 Étienne Nadji
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""
Mathematics related values, functions and enumerations.
"""

# Imports ===============================================================#

import enum
import math

from typing import Union, Literal, Optional

import pyscribus.model.document as document

# Global variables / annotations ========================================#

__author__ = "Etienne Nadji <etnadji@eml.cc>"

# 1 pica point = 25,4/72 millimeters
PICA_TO_MM = 25.4 / 72

# 1 inch = 25,4 millimeters
INCH_TO_MM = 25.4

# 1 cicero = 1.066 picas
CICERO_TO_PICA = 1.066

ConvertableToPicas = Literal["mm", "in", "c"]

Axis = Literal["x", "y"]

# Classes ===============================================================#

class FloatEnum(enum.Enum):
    """
    Enum class usable with float() function / method.
    """

    def __float__(self):
        if isinstance(self.value, float):
            return self.value

        return float(self.value)

# Functions =============================================================#

def truncate(number, digits) -> float:
    """
    Truncate value `number` to X `digits` digits.

    From Stack Overflow.

    <https://stackoverflow.com/questions/8595973/truncate-to-three-decimals-in-python>

    :type number: float
    :type digits: int
    :rtype: float
    """

    dec_number = len(str(number).split('.')[1])

    if dec_number <= digits:
        return number

    stepper = 10.0 ** digits

    return math.trunc(stepper * number) / stepper


def necessary_float(f: float) -> Union[float, int]:
    """
    Return **integer** if float f has no decimals, else returns **float**.

    :type f: float
    :param f: Float value
    :rtype: int,float
    :returns: Integer if float f has no decimals, else float.
    """

    if float(f) == int(f):
        return int(f)

    return float(f)

# Unit to picas =========================================================#

def cicero(ciceros: Union[int, float]) -> float:
    """
    Returns ciceros in pica points.

    :type inches: int,float
    :param ciceros: Ciceros
    :rtype: float
    """
    if isinstance(ciceros, int):
        ciceros = float(ciceros)

    return ciceros / CICERO_TO_PICA


def inch(inches: Union[int, float]) -> float:
    """
    Returns inches in pica points.

    :type inches: int,float
    :param inches: Inches
    :rtype: float
    """
    if isinstance(inches, int):
        inches = float(inches)

    return mm(inches / INCH_TO_MM)


def mm(millimeters: Union[int, float]) -> float:
    """
    Returns millimeters in pica points.

    :type millimeters: int,float
    :param millimeters: Milimeters
    :rtype: float
    """
    if isinstance(millimeters, int):
        millimeters = float(millimeters)

    return millimeters / PICA_TO_MM


def to_pica(value: float, unit: ConvertableToPicas) -> float:
    """
    Return value ``value`` in original unit ``unit`` in pica points.

    :rtype: float
    """

    value = float(value)

    if unit == "in":
        return inch(value)
    if unit == "mm":
        return mm(value)
    if unit == "c":
        return cicero(value)

    raise ValueError(f"Unit {unit} not known by picas conversion function.")

# Picas to unit =========================================================#

def to_mm(picas: Union[int, float]) -> float:
    """
    Returns picas in pica points.

    :type picas: int,float
    :param picas: Picas
    :rtype: float
    """
    if isinstance(picas, int):
        picas = float(picas)

    return picas * PICA_TO_MM


def to_inch(picas: Union[int, float]) -> float:
    """
    Returns picas in inches.

    :type picas: int,float
    :param picas: Picas
    :rtype: float
    """
    if isinstance(picas, int):
        picas = float(picas)

    return to_mm(picas * INCH_TO_MM)


def to_cicero(picas: Union[int, float]) -> float:
    """
    Returns picas in ciceros.

    :type inches: int,float
    :param picas: Picas
    :rtype: float
    """
    if isinstance(picas, int):
        picas = float(picas)

    return picas * CICERO_TO_PICA


def to_unit(value: float, unit: ConvertableToPicas) -> float:
    """
    Return value ``value`` in picas to value in ``unit``.

    :rtype: float
    """

    value = float(value)

    if unit == "in":
        return to_inch(value)
    if unit == "mm":
        return to_mm(value)
    if unit == "c":
        return to_cicero(value)

    raise ValueError(
        f"Unit {unit} not known by conversion from picas function."
    )

# Coordinate in unit to picas ===========================================#

def cicero_coord(ciceros: Union[int, float], scratch_margin: float) -> float:
    """
    Convert cicero coordinate ``cicero`` into picas and applies the
    Scribus' scratchspace's margin ``scratch_margin``

    :type ciceros: int,float
    :param ciceros: Ciceros
    :type scratch_margin: float
    :param scratch_margin: Scribus' scratchspace' margin to apply (in picas).
    :returns: Coordinates in picas.
    :rtype: float
    """
    return cicero(ciceros) + scratch_margin


def inch_coord(inches: Union[int, float], scratch_margin: float) -> float:
    """
    Convert inches coordinate ``inches`` into picas and applies the
    Scribus' scratchspace's margin ``scratch_margin``

    :type inches: int,float
    :param inches: Inches
    :type scratch_margin: float
    :param scratch_margin: Scribus' scratchspace' margin to apply (in picas).
    :returns: Coordinates in picas.
    :rtype: float
    """
    return inch(inches) + scratch_margin


def mm_coord(millimeters: Union[int, float], scratch_margin: float) -> float:
    """
    Convert millimeters coordinate ``millimeters`` into picas and applies the
    Scribus' scratchspace's margin ``scratch_margin``

    :type millimeters: int,float
    :param millimeters: Milimeters
    :type scratch_margin: float
    :param scratch_margin: Scribus' scratchspace' margin to apply (in picas).
    :returns: Coordinates in picas.
    :rtype: float
    """
    # A millimeter coordinate TO PICAS is calculated as
    #
    #   (MilimetersAmount / OnePicaInMilimeters) + ScratchMargin
    #   => (millimeters / (25.4/72)) + ScratchMargin
    #
    # Take @ScratchLeft as scratch_margin for X
    # Take @ScratchTop as scratch_margin for Y
    return mm(millimeters) + scratch_margin


def to_pica_coord(
    value: float,
    unit: ConvertableToPicas,
    scratch_margin: float
) -> float:
    """
    Return value ``value`` coordinate in original unit ``unit`` in pica points.

    :rtype: float
    """

    value = float(value)

    if unit == "in":
        return inch_coord(value, scratch_margin)
    if unit == "mm":
        return mm_coord(value, scratch_margin)
    if unit == "c":
        return cicero_coord(value, scratch_margin)

    raise ValueError(
        f"Unit {unit} not known by conversion to picas coordinate function."
    )

# Coordinate in picas to unit ===========================================#

def to_mm_coord(picas: Union[int, float], scratch_margin: float) -> float:
    """
    Convert picas coordinate ``picas`` into milimeters and applies the
    Scribus' scratchspace's margin ``scratch_margin``

    :type picas: int,float
    :param picas: Picas
    :type scratch_margin: float
    :param scratch_margin: Scribus' scratchspace' margin to apply (in picas).
    :returns: Coordinates in millimeters.
    :rtype: float
    """
    # A millimeter coordinate FROM PICAS is calculated as
    #
    #   (PicasAmount - ScratchMargin) * OnePicaInMilimeters
    #   => (picas - ScratchMargin) * (25.4/72)
    #
    # Take @ScratchLeft for X
    # Take @ScratchTop for Y
    return to_mm(picas - scratch_margin)


def to_inch_coord(picas: Union[int, float], scratch_margin: float) -> float:
    """
    Convert picas coordinate ``picas`` into inches and applies the
    Scribus' scratchspace's margin ``scratch_margin``

    :type picas: int,float
    :param picas: Picas
    :type scratch_margin: float
    :param scratch_margin: Scribus' scratchspace' margin to apply (in picas).
    :returns: Coordinates in inches.
    :rtype: float
    """
    return to_inch(picas - scratch_margin)


def to_cicero_coord(picas: Union[int, float], scratch_margin: float) -> float:
    """
    Convert picas coordinate ``picas`` into ciceros and applies the
    Scribus' scratchspace's margin ``scratch_margin``

    :type picas: int,float
    :param picas: Picas
    :type scratch_margin: float
    :param scratch_margin: Scribus' scratchspace' margin to apply (in picas).
    :returns: Coordinates in ciceros.
    :rtype: float
    """
    return to_cicero(picas - scratch_margin)


def to_unit_coord(
    value: float,
    unit: ConvertableToPicas,
    scratch_margin: float
) -> float:
    """
    Return value ``value`` coordinate in picas points in unit ``unit``.

    :rtype: float
    """

    value = float(value)

    if unit == "in":
        return to_inch_coord(value, scratch_margin)
    if unit == "mm":
        return to_mm_coord(value, scratch_margin)
    # if unit == "c":
        # return cicero(value)

    raise ValueError(
        f"Unit {unit} not known by conversion from picas coordinates function."
    )

# Converter classes =====================================================#

class PicaConverter:
    """
    Converter into picas/unit class.

    Define a original units, output picas.
    """

    convertable_units = {
        "millimeters": "mm",
        "inches": "in",
        "ciceros": "c",
    }

    def __init__(
        self,
        original_unit: Optional[str] = None,
        model: Optional[document.Document] = None
    ):
        self.original_unit = original_unit
        self.model = model

    # --------------------------------------------------------------------

    def set_unit(self, original_unit: Optional[str] = None) -> bool:
        for long, short in PicaConverter.convertable_units.items():

            if original_unit == short:
                self.original_unit = long
                break

            if original_unit == long:
                self.original_unit = long
                break

        if self.original_unit is None:
            self.original_unit = None
            return None

        return self.original_unit

    # Conversion of coordinates ------------------------------------------

    def __scratch_margin(self, axis: Axis) -> float:
        """
        Return the correct scratchspace margin value according to the axis
        ``axis`` given.

        :type axis: Axis
        :param Axis: Axis of the coordinate. Must be 'x' or 'y'.
        :rtype: float
        :returns: Scratchspace margin (in picas).
        """

        scratch_margin = None

        if axis == "x":
            scratch_margin = self.model.document.scratchspace["left"].value
        elif axis == "y":
            scratch_margin = self.model.document.scratchspace["top"].value
        else:
            raise ValueError(
                "Conversion of coordinate in picas points: invalid axis. "
                "Axis possible values: 'x', 'y'."
            )

        return scratch_margin

    def picas_coord(self, axis: Axis, value: Union[int, float]) -> float:
        """
        Return coordinate value ``value`` in pica points.

        :type axis: Axis
        :param Axis: Axis of the coordinate. Must be 'x' or 'y'.
        :type value: Union[int, float]
        :param value: Value in defined unit, or picas, to convert.
        :rtype: float
        :returns: Coordinate in picas.
        """

        # If the unit is in picas, there is nothing to do.
        if self.original_unit is None:
            return value

        scratch_margin = self.__scratch_margin(axis)

        return to_pica_coord(
            value,
            PicaConverter.convertable_units[self.original_unit],
            scratch_margin
        )

    def unit_coord(self, axis: Axis, value: Union[int, float]) -> float:
        """
        Return coordinate value ``value`` in the unit defined.

        :type axis: Axis
        :param Axis: Axis of the coordinate. Must be 'x' or 'y'.
        :type value: Union[int, float]
        :param value: Value in defined unit, or picas, to convert.
        :rtype: float
        :returns: Coordinate in defined unit.
        """

        # If there is not original unit defined, then the returned value
        # must be in picas.
        if self.original_unit is None:
            return value

        scratch_margin = self.__scratch_margin(axis)

        return to_unit_coord(
            value,
            PicaConverter.convertable_units[self.original_unit],
            scratch_margin
        )

    # Conversion of other values -----------------------------------------

    def picas(self, value: Union[int, float]) -> float:
        """
        Return value ``value`` in pica points.

        :type value: Union[int, float]
        :param value: Value in defined unit, or picas, to convert.
        :rtype: float
        :returns: Value in picas.
        """

        if self.original_unit is None:
            return value

        return to_pica(
            value,
            PicaConverter.convertable_units[self.original_unit]
        )

    def unit(self, value: Union[int, float]) -> float:
        """
        Return value ``value`` in picas in the unit defined.

        :type value: Union[int, float]
        :param value: Value in defined unit, or picas, to convert.
        :rtype: float
        :returns: Value in defined unit.
        """
        if self.original_unit is None:
            return value

        return to_unit(
            value,
            PicaConverter.convertable_units[self.original_unit]
        )

# vim:set shiftwidth=4 softtabstop=4:
