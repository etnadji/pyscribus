#!/usr/bin/python3
# -*- coding:Utf-8 -*-

# PyScribus, python library for Scribus SLA
# Copyright (C) 2020-2023 Étienne Nadji
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""
PyScribus classes for notes
"""

# Imports ===============================================================#

# To avoid Sphinx complaints from methods annotations referencing same class
from __future__ import annotations

import copy

from typing import Union, NoReturn, Literal

import lxml
import lxml.etree as ET

import pyscribus.model.exceptions as exceptions

from pyscribus.model.common.xml import *

# Variables globales ====================================================#

__author__ = "Etienne Nadji <etnadji@eml.cc>"

# Classes ===============================================================#


class Note(PyScribusElement):
    """
    Note element (Note)
    """

    def __init__(self, doc_parent=False):
        PyScribusElement.__init__(self)

        self.style = ""
        self.parent_mark = ""
        self.parent_frame = ""

        self.doc_parent = doc_parent

        self.text = NoteText(parent_note=self)

    def fromdefault(self) -> NoReturn:
        self.style = "Default"
        self.text = NoteText()
        self.text.fromdefault()

    def fromxml(self, xml: ET.Element) -> bool:

        if xml.tag != "Note":
            return False

        style = xml.get("NStyle")
        parent_mark = xml.get("Master")

        if style is not None:
            self.style = style

        if parent_mark is not None:
            self.parent_mark = parent_mark

        if "Text" in xml.attrib:
            text = NoteText()
            success = text.fromxml(xml)

            if success:
                self.text = text

        return True

    def toxml(self) -> ET.Element:
        xml = ET.Element("Note")

        xml.attrib["Master"] = self.parent_mark
        xml.attrib["NStyle"] = self.style
        xml.attrib["Text"] = self.text.toxmlstr()

        return xml


class NoteTextDefaultStyle(PyScribusElement):
    """
    <defaultstyle> in note content code.
    """

    def __init__(self):
        PyScribusElement.__init__(self)

        self.parent = None

    def fromxml(self, xml: ET.Element) -> bool:
        parent = xml.get("parent")

        if parent is not None:
            if parent:
                self.parent = parent

        return True

    def __repr__(self) -> str:
        rpr = "NoteTextDefaultStyle("

        if self.parent is not None:
            rpr += f"parent='{self.parent}'"

        rpr += ")"

        return rpr

    def toxml(self) -> ET.Element:
        xml = ET.Element("defaultstyle")

        if self.parent is not None:
            xml.attrib["parent"] = self.parent

        return xml


class NoteTextStyle(PyScribusElement):
    """
    <style> in note content code <p>.
    """

    def __repr__(self) -> str:
        return "NoteTextStyle()"

    def fromxml(self, xml: ET.Element) -> bool:

        if xml.tag != "style":
            return False

        return True

    def toxml(self) -> ET.Element:
        xml = ET.Element("style")

        return xml


class NoteTextCharStyle(PyScribusElement):
    """
    <charstyle> in note content code <span>.
    """

    def __init__(self, **kwargs):
        PyScribusElement.__init__(self)

        self.font = None
        self.features = {
            "inherit": False,
            "underline": False,
        }

        if kwargs:
            self._quick_setup(kwargs)

    def _quick_setup(self, settings: dict) -> NoReturn:
        """
        :type settings: dict
        :param settings: Kwargs dictionnary
        """

        if settings:
            PyScribusElement._quick_setup(self, settings)

            for setting_name, setting_value in settings.items():

                if setting_name == "features":
                    self.set_features(setting_value)

                if setting_name == "font":
                    self.font = setting_value

    def __repr__(self) -> str:
        rpr = "NoteTextCharStyle("

        args = []

        features = self.active_features()

        if features:
            args.append(f"features='{features}'")

        if self.font is not None:
            args.append(f"font='{self.font}'")

        rpr += ", ".join(args)

        rpr += ")"

        return rpr

    def active_features(self) -> str:
        return " ".join(
            [
                feature_name
                for feature_name, feature_active in self.features.items()
                if feature_active
            ]
        )

    def set_features(self, features: str) -> bool:
        if not features:
            return False

        for feature in features.split():
            self.features[feature] = True

        return True

    def fromxml(self, xml: ET.Element) -> bool:
        if xml.tag != "charstyle":
            return False

        if (features := xml.get("Features")) is not None:
            self.set_features(features)

        if (font := xml.get("Font")) is not None:
            if font:
                self.font = font

        return True

    def toxml(self) -> ET.Element:
        xml = ET.Element("charstyle")

        features = self.active_features()

        if features:
            xml.attrib["Features"] = features

        if self.font is not None:
            xml.attrib["Font"] = self.font

        return xml


class NoteTextSpan(PyScribusElement):
    """
    <span> in note content code <p>.
    """

    def __init__(self, **kwargs):
        PyScribusElement.__init__(self)

        self.sequence = []

        if kwargs:
            self._quick_setup(kwargs)

    def _quick_setup(self, settings: dict) -> NoReturn:
        """
        :type settings: dict
        :param settings: Kwargs dictionnary
        """

        if settings:
            PyScribusElement._quick_setup(self, settings)

            for setting_name, setting_value in settings.items():

                if setting_name == "sequence":
                    self.sequence = setting_value

    def __repr__(self) -> str:
        rpr = "NoteTextSpan("

        if self.sequence:
            rpr += "sequence=["

            elements = []

            for element in self.sequence:
                if isinstance(element, str):
                    elements.append(f"'{element}'")
                else:
                    elements.append(element.__repr__())

            rpr += ", ".join(elements)

            rpr += "]"

        rpr += ")"

        return rpr

    def fromxml(self, xml: ET.Element) -> bool:
        if xml.tag != "span":
            return False

        if len(xml):
            for element in xml:

                if element.tag == "charstyle":
                    charstyle = NoteTextCharStyle()
                    success = charstyle.fromxml(element)

                    if success:
                        self.sequence.append(charstyle)

                if element.tail:
                    self.sequence.append(element.tail)
        else:
            self.sequence.append(xml.text)

        return True

    def toxml(self) -> ET.Element:
        xml = ET.Element("span")

        children = []

        for idx, element in enumerate(self.sequence):

            if isinstance(element, NoteTextCharStyle):
                children.append(element.toxml())

            if isinstance(element, str):
                if idx == 0:
                    xml.text = element
                    continue

                if isinstance(self.sequence[idx - 1], NoteTextCharStyle):
                    children[-1].tail = element
                    continue

        for child in children:
            xml.append(child)

        return xml


class NoteTextParagraph(PyScribusElement):
    """
    <p> in note content code.
    """

    def __init__(self, **kwargs):
        PyScribusElement.__init__(self)

        self.sequence = []

        if kwargs:
            self._quick_setup(kwargs)

    def _quick_setup(self, settings: dict) -> NoReturn:
        """
        :type settings: dict
        :param settings: Kwargs dictionnary
        """

        if settings:
            PyScribusElement._quick_setup(self, settings)

            for setting_name, setting_value in settings.items():

                if setting_name == "sequence":
                    self.sequence = setting_value

    def __repr__(self) -> str:
        rpr = "NoteTextParagraph("

        if self.sequence:
            rpr += "sequence=["

            rpr += ", ".join(element.__repr__() for element in self.sequence)

            rpr += "]"

        rpr += ")"

        return rpr

    def fromxml(self, xml: ET.Element) -> bool:

        for element in xml:

            if element.tag == "style":
                style = NoteTextStyle()
                success = style.fromxml(element)

                if success:
                    self.sequence.append(style)

            if element.tag == "span":
                span = NoteTextSpan()
                success = span.fromxml(element)

                if success:
                    self.sequence.append(span)

        return True

    def toxml(self) -> ET.Element:
        xml = ET.Element("p")

        for element in self.sequence:
            xml.append(element.toxml())

        return xml


class NoteText(PyScribusElement):
    """
    Note element text (Note/@Text).

    Note element text consists in a full XML file dumped into @Text attribute
    of Note, hence the need for a specific class.
    """

    empty_text = '&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;' \
    '&lt;SCRIBUSTEXT &gt;&lt;defaultstyle /&gt;' \
    '&lt;p &gt;&lt;style /&gt;&lt;span &gt;' \
    '&lt;charstyle Features=&quot;inherit &quot; /&gt;' \
    '&lt;/span&gt;&lt;/p&gt;&lt;/SCRIBUSTEXT&gt;&#10;'

    empty_pattern = '&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;' \
    '&lt;SCRIBUSTEXT &gt;&lt;defaultstyle /&gt;' \
    '&lt;p &gt;&lt;style /&gt;&lt;span &gt;' \
    '&lt;charstyle Features=&quot;inherit &quot; /&gt;' \
    '&lt;/span&gt;&lt;/p&gt;{0}&lt;/SCRIBUSTEXT&gt;&#10;'

    normalizers = [
        ["&gt;&lt;", "><"],
        ["&gt;", ">"],
        ["&lt;", "<"],
        ["&quot;", '"'],
        # NOTE Keep the following line at the end of the list.
        ["&#10;", ""],
    ]

    def __init__(self, parent_note=False, **kwargs):
        PyScribusElement.__init__(self)

        self.sequence = []

        # We need to link NoteText to its note element (Note), because Note
        # is linked with pyscribus.document.Document.
        self.parent_note = parent_note

        if kwargs:
            self._quick_setup(kwargs)

    def _quick_setup(self, settings: dict) -> NoReturn:
        """
        :type settings: dict
        :param settings: Kwargs dictionnary
        """

        if settings:
            PyScribusElement._quick_setup(self, settings)

            for setting_name, setting_value in settings.items():

                if setting_name == "sequence":
                    self.sequence = setting_value

    def sequence_from_xml(self, xml: ET.Element):
        """
        Deconstruct a note text string as XML element to a sequence of
        PyScribus classes.

        :type xml: ET.Element
        """

        for main in xml:

            if main.tag == "defaultstyle":
                default = NoteTextDefaultStyle()
                success = default.fromxml(main)

                if success:
                    self.sequence.append(default)

            if main.tag == "p":
                new_para = NoteTextParagraph()
                success = new_para.fromxml(main)

                if success:
                    self.sequence.append(new_para)

    def fromxml(self, xml: ET.Element) -> bool:

        if xml.tag != "Note":
            return False

        content = xml.get("Text")

        if content is None:
            return False

        content = self.normalize_text(content, "text")

        content = content.replace('<?xml version="1.0" encoding="UTF-8"?>', "")

        self.sequence_from_xml(ET.fromstring(content))

        return True

    def normalize_text(self, text: str, direction: Literal["xml", "text"]):
        """
        Normalize note content text towards a direction, either XML or text.

        :type text: str
        :param text: Text content
        :type direction: Literal["xml", "text"]
        :param direction: Direction of normalization
        """

        new_text = copy.deepcopy(text)

        # => Text -----------------------------------------------------------

        if direction == "text":
            for text_in, text_out in NoteText.normalizers:
                new_text = new_text.replace(text_in, text_out)

            # Remove encoding declaration because lxml.etree.fromstring does
            # not support it in unicode strings.
            new_text = new_text.replace(
                '<?xml version="1.0" encoding="UTF-8"?>', ""
            )

            return new_text

        # => XML ------------------------------------------------------------

        for text_out, text_in in NoteText.normalizers:
            # We skip the line feed replacement because this is only used
            # in direction of text.
            if text_out == NoteText.normalizers[-1][0]:
                continue

            new_text = new_text.replace(text_in, text_out)

        # Add the line feed
        new_text += "&#10;"

        return new_text

    def toxml(self) -> str:
        """
        Alias of toxmlstr()

        :rtype: str
        """
        return self.toxmlstr()

    def toxmlstr(self) -> str:
        """
        Returns note text content as a XML string acceptable by Scribus.

        :rtype: str
        """
        if self.sequence is None:
            return ""
        if not self.sequence:
            return ""

        xml = ET.Element("SCRIBUSTEXT")

        for element in self.sequence:
            xml.append(element.toxml())

        # Scribus doesn't understands escaped-as-entities Unicode characters
        # and crashes if it encounters one.
        xml_string = ET.tounicode(xml)

        xml_string = '<?xml version="1.0" encoding="UTF-8"?>' + xml_string
        xml_string = self.normalize_text(xml_string, "xml")

        return xml_string


class NoteFrame(PyScribusElement):
    """
    Note frame (NotesFrames/FOOTNOTEFRAME).
    """

    def __init__(self):
        PyScribusElement.__init__(self)

        # NOTE @myID
        # The page object where the note content is located
        self.own_frame_id = None

        # NOTE @MasterID
        # The page object with the story where note references are located
        self.story_frame_id = None

        # NOTE @NSname
        self.note_style = None

    def fromxml(self, xml: ET.Element) -> bool:

        if xml.tag != "FOOTNOTEFRAME":
            return False

        note_style = xml.get("NSname")
        own_frame = xml.get("myID")
        story_frame = xml.get("MasterID")

        if note_style is not None:
            self.note_style = note_style

        if own_frame is not None:
            self.own_frame_id = own_frame

        if story_frame is not None:
            self.story_frame_id = story_frame

        if story_frame is None or own_frame is None:

            raise exceptions.InsaneSLAValue(
                "Note frame has no page object ID or\
                 no parent page object ID."
            )

        return True

    def toxml(self) -> ET.Element:
        xml = ET.Element("FOOTNOTEFRAME")

        if self.note_style is not None:
            xml.attrib["NSname"] = self.note_style

        if self.own_frame_id is not None:
            xml.attrib["myID"] = self.own_frame_id

        if self.story_frame_id is not None:
            xml.attrib["MasterID"] = self.story_frame_id

        return xml

    def fromdefault(self) -> NoReturn:
        self.note_style = "Default"


# vim:set shiftwidth=4 softtabstop=4 spl=en:
