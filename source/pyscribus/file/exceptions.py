#!/usr/bin/python3
# -*- coding:Utf-8 -*-

# PyScribus, python library for Scribus SLA
# Copyright (C) 2020-2023 Étienne Nadji
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""
PyScribus

Scribus file. Exceptions classes and decorators related to them.
"""

# Imports ===============================================================#

import functools

# Variables globales ====================================================#

__author__ = "Etienne Nadji <etnadji@eml.cc>"

# Classes ===============================================================#


class NoDocumentError(Exception):
    """Raised when a model has no document."""


class NoModelError(Exception):
    """Raised when a ScribusFile has no underlying model."""


class PageNotFound(Exception):
    """Raised when the user asks for a page that can't be found."""


class LayerNotFound(Exception):
    """Raised when the user asks for a layer that can't be found."""


# Decorators ============================================================#


def has_model(func):
    """
    Decorator. Raise NoModelError if ScribusFile has no model.
    """

    @functools.wraps(func)
    def wrapper(self, *args, **kw):
        if self.model is None:
            raise NoModelError(
                "No underlying model found in this ScribusFile."
            )

        return func(self, *args, **kw)

    return wrapper


def has_document(func):
    """
    Decorator. Raise NoDocumentError if ScribusFile has no model.
    """

    @functools.wraps(func)
    def wrapper(self, *args, **kw):
        if self.model is None:
            raise NoModelError(
                "No underlying model found in this ScribusFile."
            )

        if self.model.document is None:
            raise NoDocumentError()

        return func(self, *args, **kw)

    return wrapper


def has_object_model(func):
    """
    Decorator. Raise NoModelError if the page object has no underlying
    model.
    """

    @functools.wraps(func)
    def wrapper(self, *args, **kw):
        if self.object is None:
            raise NoModelError("No underlying class model found.")

        return func(self, *args, **kw)

    return wrapper


# vim:set shiftwidth=4 softtabstop=4:
