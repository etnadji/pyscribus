#!/usr/bin/python3
# -*- coding:Utf-8 -*-

# PyScribus, python library for Scribus SLA
# Copyright (C) 2020-2023 Étienne Nadji
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""
PyScribus

Scribus file. Classes for page objects.
"""

# Imports ===============================================================#

# Standard library ---------------------------------------------

# To avoid Sphinx complaints from methods annotations referencing same class
from __future__ import annotations

from typing import Union, Any, NoReturn, Literal

from pathlib import Path

import fnmatch
import uuid

# PyScribus model ----------------------------------------------

import pyscribus.model.dimensions as dimensions
import pyscribus.model.pageobjects as pageobjects
import pyscribus.model.itemattribute as itemattribute

# PyScribus file -----------------------------------------------

import pyscribus.file as pyf
import pyscribus.file.colors as colors
import pyscribus.file.stories as stories
import pyscribus.file.document as document
import pyscribus.file.exceptions as pyfe

# Global variables / Annotations ========================================#

__author__ = "Etienne Nadji <etnadji@eml.cc>"

StringOrNone = Union[str, None]
AttributeType = Literal["boolean", "integer", "double", "string", "none"]
AttributeValue = Union[None, str, bool, int, float]
ColorOrNothing = Union[str, colors.Color, bool, None]

# Classes ===============================================================#


class ObjectAttribute:
    """
    Attribute of a page object.
    """

    def __init__(self, **kwargs):
        self.object = None

        if (new := kwargs.get("new")) is not None:
            if bool(new):
                self._new(**kwargs)

        if (model_object := kwargs.get("model_object")) is not None:
            self.object = model_object

    def _new(self, **kwargs) -> NoReturn:
        """
        Initialising object attribute from scratch.
        """
        self._new_model(**kwargs)
        self._new_settings(**kwargs)

    def _new_model(self, **kwargs) -> NoReturn:
        """ """
        self.object = itemattribute.PageObjectAttribute()
        self.object.att_type = None

    def _new_settings(self, **kwargs) -> NoReturn:
        # When setting a value, we need the attribute type, so it is
        # searched & set first.

        if (att_type := kwargs.get("att_type")) is not None:
            self.att_type = att_type

        for setting_name, setting_value in kwargs.items():
            if setting_name == "att_type":
                continue

            if setting_name == "name":
                self.name = setting_value
            if setting_name == "value":
                self.value = setting_value

    @property
    @pyfe.has_object_model
    def name(self) -> str:
        """Attribute name."""
        return self.object.name

    @name.setter
    @pyfe.has_object_model
    def name(self, value: str) -> str:
        self.object.name = value

    @property
    @pyfe.has_object_model
    def value(self) -> AttributeValue:
        """Attribute value."""

        if self.object.attribute_type == "none":
            return None

        return self.object.as_python()

    @value.setter
    @pyfe.has_object_model
    def value(self, value: AttributeValue) -> NoReturn:
        """Attribute value (setter)."""

        if self.object.attribute_type == "none":
            self.object.value = ""

        if self.object.attribute_type == "integer":
            if isinstance(value, int):
                self.object.value = str(value)
            else:
                raise TypeError(
                    "Page object attribute expected an integer (int). "
                    f"Got '{value}'."
                )

        if self.object.attribute_type == "double":
            if isinstance(value, float):
                self.object.value = str(value)
            else:
                raise TypeError(
                    "Page object attribute expected a double (float). "
                    f"Got '{value}'."
                )

        if self.object.attribute_type == "string":
            if isinstance(value, str):
                self.object.value = str(value)
            else:
                raise TypeError(
                    "Page object attribute expected a string (str). "
                    f"Got '{value}'."
                )

        if self.object.attribute_type == "boolean":
            if isinstance(value, bool):
                self.object.value = {True: "true", False: "false"}[value]
            else:
                raise TypeError(
                    "Page object attribute expected a boolean (bool). "
                    f"Got '{value}'."
                )

    @property
    @pyfe.has_object_model
    def att_type(self) -> str:
        """
        Attribute type.

        Can be: ``'boolean'``, ``'integer'``, ``'double'``, ``'string'``,
        ``'none'``.
        """
        return self.object.attribute_type

    @att_type.setter
    @pyfe.has_object_model
    def att_type(self, value: Union[AttributeType, None]) -> str:
        if value is None:
            self.object.attribute_type = "none"
        elif value in itemattribute.ItemAttribute.attrib_types:
            self.object.attribute_type = value
        else:
            raise ValueError(
                "Page object attribute type possible values: "
                "'boolean', 'integer', 'double', 'string', 'none'. "
                f"Got '{value}'."
            )


class PageObject:
    def __init__(self, file, **kwargs):
        self.file = file
        self.object = None
        self._converter = None

        if (converter := kwargs.get("converter")) is not None:
            self._converter = converter

        if (new := kwargs.get("new")) is not None:
            if bool(new):
                self._new(**kwargs)

        if (model_object := kwargs.get("model_object")) is not None:
            self.object = model_object

    def _picas(self, value: Union[int, float]) -> float:
        if self._converter is None:
            return value

        return self._converter.picas(value)

    def _unit(self, value: Union[int, float]) -> float:
        if self._converter is None:
            return value

        return self._converter.unit(value)

    def _picas_coord(self, axis, value: Union[int, float]) -> float:
        if self._converter is None:
            return value

        return self._converter.picas_coord(axis, value)

    def _unit_coord(self, axis, value: Union[int, float]) -> float:
        if self._converter is None:
            return value

        return self._converter.unit_coord(axis, value)

    def _new(self, **kwargs) -> NoReturn:
        """
        Initialising page object from scratch.
        """
        self._new_model(**kwargs)
        self.object.object_id = str(str(uuid.uuid4()))
        self._new_settings(**kwargs)

    def _new_settings(self, **kwargs) -> NoReturn:
        """
        Handle kwargs values to initialize values that doesn't need to
        be initialized with the model in ``_new_model`` method, for example,
        with class properties instead of altering values in the underlying
        model.

        In subclasses, refering to this method when coding ``_new_settings``
        again is mandatory.
        """

        for setting_name, setting_value in kwargs.items():
            if setting_name == "name":
                self.name = setting_value
            if setting_name == "pos_x":
                self.pos_x = setting_value
            if setting_name == "pos_y":
                self.pos_y = setting_value
            if setting_name == "height":
                self.height = setting_value
            if setting_name == "width":
                self.width = setting_value
            if setting_name == "frame_fill":
                self.frame_fill = setting_value
            if setting_name == "frame_stroke":
                self.frame_stroke = setting_value

    def _new_model(self, **kwargs) -> NoReturn:
        """
        Placeholder method for subclasses.

        Create the underlying page object model here and attribute it to
        ``object`` attribute.
        """

    # Page object attributes ---------------------------------------------

    @property
    @pyfe.has_object_model
    def attributes(self) -> list[ObjectAttribute]:
        """
        Page object attributes.

        :rtype: list[ObjectAttribute]
        """
        # Attributes may not have names, so we can't return a dictionary,
        # which would be nicer…
        return [
            ObjectAttribute(model_object=attribute)
            for attribute in self.object.attributes
        ]

    def get_attribute(self, name: str) -> Union[ObjectAttribute, bool]:
        for attribute in self.attributes:
            if attribute.name == name:
                return attribute

        return False

    def has_attribute(self, name: str) -> bool:
        """
        Does this page object has an attributed called ``name``?

        :rtype: bool
        """
        return self.get_attribute(name)

    def add_attribute(
        self,
        att_type: Union[AttributeType, None],
        att_name: str,
        att_value: AttributeValue,
    ) -> ObjectAttribute:
        """
        Add an attribute to this page object.

        :type att_type: Union[AttributeType, None]
        :param att_type: Type of the attribute value.
        :type att_name: str
        :param att_name: Name of the attribute.
        :type att_value: AttributeValue
        :param att_value: Value of the attribute.
        :returns: Created page object attribute.
        :rtype: ObjectAttribute
        """
        if self.has_attribute(att_name):
            raise ValueError(f"The attribute '{att_name}' already exists.")

        new_att = ObjectAttribute(
            new=True,
            name=att_name,
            value=att_value,
            att_type=att_type,
        )

        self.object.attributes.append(new_att.object)

        return new_att

    # Name ---------------------------------------------------------------

    @property
    @pyfe.has_object_model
    def name(self) -> str:
        """Page object name."""
        return self.object.name

    @name.setter
    @pyfe.has_object_model
    def name(self, value: str) -> NoReturn:
        self.object.name = value

    # Coordinates --------------------------------------------------------

    @property
    @pyfe.has_object_model
    def pos_x(self) -> float:
        """Page object X coordinate of the top-level corner."""
        return self._unit_coord("x", self.object.box.getx("top-left"))

    @pos_x.setter
    @pyfe.has_object_model
    def pos_x(self, value: float) -> NoReturn:
        self.object.box.setx("top-left", self._picas_coord("x", value))

    @property
    @pyfe.has_object_model
    def pos_y(self) -> float:
        """Page object Y coordinate of the top-level corner."""
        return self._unit_coord("y", self.object.box.gety("top-left"))

    @pos_y.setter
    @pyfe.has_object_model
    def pos_y(self, value: float) -> NoReturn:
        self.object.box.sety("top-left", self._picas_coord("y", value))

    # Welding ------------------------------------------------------------

    # Size ---------------------------------------------------------------

    @property
    @pyfe.has_object_model
    def width(self) -> float:
        """Page object width."""
        return self._unit(self.object.box.dims["width"].value)

    @width.setter
    @pyfe.has_object_model
    def width(self, value: float) -> NoReturn:
        self.object.box.dims["width"].value = self._picas(value)

    @property
    @pyfe.has_object_model
    def height(self) -> float:
        """Page object height."""
        return self._unit(self.object.box.dims["height"].value)

    @height.setter
    @pyfe.has_object_model
    def height(self, value: float) -> NoReturn:
        self.object.box.dims["height"].value = self._picas(value)

    # Outline ------------------------------------------------------------

    @property
    @pyfe.has_object_model
    def frame_fill(self) -> str:
        """Page object's frame's background color."""
        return self.object.outline["fill"]

    @frame_fill.setter
    @pyfe.has_object_model
    def frame_fill(self, value: ColorOrNothing) -> NoReturn:
        if isinstance(value, bool) and value:
            value = "Black"
        if not value or value is None:
            value = None

        if value is None:
            self.object.outline["fill"] = value
        else:
            self.object.outline["fill"] = str(value)

    @property
    @pyfe.has_object_model
    def frame_stroke(self) -> str:
        """Page object's frame's outline color."""
        return self.object.outline["stroke"]

    @frame_stroke.setter
    @pyfe.has_object_model
    def frame_stroke(self, value: ColorOrNothing) -> NoReturn:
        if isinstance(value, bool) and value:
            value = "Black"
        if not value or value is None:
            value = None

        if value is None:
            self.object.outline["stroke"] = value
        else:
            self.object.outline["stroke"] = str(value)

    # --------------------------------------------------------------------


class PolygonObject(PageObject):
    """
    Image object.

    .. note:: Create an instance of this class through
        ``PageObjects.new_polygon``.

    """

    def _new_model(self, **kwargs):
        self.object = pageobjects.new_from_type(
            "polygon",
            self.file.model,
            self.file.model.document,
            default=True,
        )

    def _new_settings(self, **kwargs) -> NoReturn:
        PageObject._new_settings(self, **kwargs)

        # No frame background, black outline ------------------------

        if kwargs.get("frame_fill") is None:
            self.frame_fill = None

        if kwargs.get("frame_stroke") is None:
            self.frame_stroke = "Black"


class ImageObject(PageObject):
    """
    Image object.

    .. note:: Create an instance of this class through ``PageObjects.new_image``.

    """

    def _new_model(self, **kwargs):
        self.object = pageobjects.new_from_type(
            "image",
            self.file.model,
            self.file.model.document,
            default=True,
        )

    def _new_settings(self, **kwargs) -> NoReturn:
        PageObject._new_settings(self, **kwargs)

        # No frame border, background, unless asked otherwise -------

        if kwargs.get("frame_fill") is None:
            self.frame_fill = None

        if kwargs.get("frame_stroke") is None:
            self.frame_stroke = None

        # -----------------------------------------------------------

        for setting_name, setting_value in kwargs.items():
            if setting_name == "path":
                self.path = setting_value

            if setting_name == "data":
                self.data = setting_value

            if setting_name == "scaling":
                self.scaling = setting_value

    # File path ----------------------------------------------------------

    @property
    @pyfe.has_object_model
    def path(self) -> Path:
        """
        Path of the image.

        :rtype: Path
        """
        return Path(self.object.filepath)

    @path.setter
    @pyfe.has_object_model
    def path(self, value: Union[str, Path]) -> NoReturn:
        self.object.filepath = str(value)

    # File data ----------------------------------------------------------

    @property
    @pyfe.has_object_model
    def data(self) -> str:
        """
        Embedded image data.

        :rtype: str
        :returns: Image data as Qt compressed base 64 string.
        """
        return self.object.data

    @data.setter
    @pyfe.has_object_model
    def data(self, value: str) -> NoReturn:
        """Embedded image data (setter)."""
        self.object.data = value

    # Image scaling ------------------------------------------------------

    @property
    @pyfe.has_object_model
    def scaling(self) -> bool:
        """
        Scaling image page to frame?

        :rtype: bool
        """
        return self.object.image_scale["type"] == "frame"

    @scaling.setter
    @pyfe.has_object_model
    def scaling(self, value: bool) -> NoReturn:
        if bool(value):
            self.object.image_scale["type"] = "free"
            self.object.image_scale["ratio"] = True
        else:
            self.object.image_scale["type"] = "frame"
            self.object.image_scale["ratio"] = False

    # --------------------------------------------------------------------


class TextObject(PageObject):
    """
    Text object.

    .. note:: Create an instance of this class through ``PageObjects.new_text``.

    """

    def __init__(self, file, **kwargs):
        PageObject.__init__(self, file, **kwargs)

    def _new_settings(self, **kwargs) -> NoReturn:
        PageObject._new_settings(self, **kwargs)

        # No frame border, background, unless asked otherwise -------

        if kwargs.get("frame_fill") is None:
            self.frame_fill = None

        if kwargs.get("frame_stroke") is None:
            self.frame_stroke = None

        # -----------------------------------------------------------

        for setting_name, setting_value in kwargs.items():
            if setting_name == "columns":
                self.columns = setting_value

            if setting_name == "columns_gap":
                self.columns_gap = setting_value

            if setting_name == "align":
                self.align = setting_value

            if setting_name == "vertical_align":
                self.vertical_align = setting_value

    def _new_model(self, **kwargs):
        default = kwargs.get("default")

        if default is None:
            default = "with-story"

        self.object = pageobjects.new_from_type(
            "text",
            self.file.model,
            self.file.model.document,
            default=default,
        )

    # Horizontal alignment -----------------------------------------------

    @property
    @pyfe.has_object_model
    def align(self) -> str:
        """
        Horizontal alignment.

        Possible values: ``left``, ``center``, ``right``, ``justify-left``,
        ``justify``.

        .. note:: this applies to the text object. the alignment of story's
            content may override this setting (a paragraph using a right
            centered text in a text object left center will be right centered).

        """
        return self.object.alignment

    @align.setter
    @pyfe.has_object_model
    def align(self, value: str) -> NoReturn:
        """Horizontal alignment (setter)."""

        if value in ["left", "center", "right", "justify-left", "justify"]:
            self.object.alignment = value
        else:
            raise ValueError(
                "Text object horizontal alignment possible values: "
                "'left', 'center', 'right', 'justify-left', 'justify'."
                f" Got '{value}'."
            )

    # Vertical alignment -------------------------------------------------

    @property
    @pyfe.has_object_model
    def vertical_align(self) -> str:
        """
        Vertical alignment.

        Possible values: ``top``, ``center``, ``bottom``.
        """
        return self.object.vertical_alignment

    @vertical_align.setter
    @pyfe.has_object_model
    def vertical_align(self, value: str) -> NoReturn:
        """Vertical alignment (setter)."""
        if value in ["top", "center", "bottom"]:
            self.object.vertical_alignment = value
        else:
            raise ValueError(
                "Text object vertical alignment possible values: "
                "'top', 'center', 'bottom'."
                f" Got '{value}'."
            )

    # Column count -------------------------------------------------------

    @property
    @pyfe.has_object_model
    def columns(self) -> int:
        """
        Number of columns.
        """
        return self.object.columns["count"]

    @columns.setter
    @pyfe.has_object_model
    def columns(self, value: int) -> NoReturn:
        """Number of columns (setter)."""
        self.object.columns["count"] = int(value)

    # Gap between columns ------------------------------------------------

    @property
    @pyfe.has_object_model
    def columns_gap(self) -> float:
        """Gap size between colums (in picas)"""
        return self._unit(self.object.columns["gap"].value)

    @columns_gap.setter
    @pyfe.has_object_model
    def columns_gap(self, value: float) -> NoReturn:
        """Gap size between colums (setter)"""
        self.object.columns["gap"].value = self._picas(value)

    # --------------------------------------------------------------------

    @property
    @pyfe.has_object_model
    def story(self) -> Union[stories.Story, None]:
        try:
            return self.object.stories[-1]
        except IndexError:
            return None

    @story.setter
    @pyfe.has_object_model
    def story(self, value) -> NoReturn:
        # Set this page object as the starting frame of the story
        value.frame = self.object.object_id
        # Update the story
        value.update()

        self.object.stories = [value.object]


# Interfaces for page objects dictionary ================================#

po_interfaces = {
    "image": ImageObject,
    "text": TextObject,
    # "line": LineObject,
    "polygon": PolygonObject,
    # "polyline": PolylineObject,
    # "textonpath": TextOnPathObject,
    # "render": RenderObject,
    # "table": TableObject,
    # "group": GroupObject,
    # "symbol": SymbolObject,
}

# PageObject interface ==================================================#


class PageObjects:
    """
    Interface for document's page objects.

    :type file: pyscribus.file.ScribusFile
    :param file: Scribus file wrapper

    :Example:

    .. code:: python

       # To avoid writing "scribus_file.pageobjects" again and again
       doc_objects = scribus_file.pageobjects

       # Filtering page objects ------------------------------------------

       templatable_text_frames = doc_objects.filter(
          object_type="text", templatable=True
       )

       on_page_two = doc_objects.filter(page_number=1)

       # Is an object on page 2 on page 1 ? No.
       is_on_page_one = doc_objects.on_page(on_page_two[0], 0)

       # Access page objects by loading order ----------------------------

       po_two = doc_objects[1]

       # To access all objects, use item property…

       all_objects = doc_objects.items

       # … or iterate on doc_objects:

       for object in doc_objects:
           print(object.name)

    """

    def __init__(self, file: pyf.ScribusFile):
        self.file = file
        self.model = file.model

    def __get_page(self, page_number: int, master: bool = False):
        """
        Get the page number ``page_number`` and the full page set (normal /
        master pages).

        :type page_number: int
        :param page_number: Page number of the page to copy.
        :type master: bool
        :param master: Use the master pages set instead of normal pages.
        :returns: Page number ``page_number`` and the full page set.
        """

        if page_number < 0:
            raise pyfe.PageNotFound("Page number asked inferior to 0.")

        if master:
            pages = document.Pages(self.file).masterpages
        else:
            pages = document.Pages(self.file).pages

        try:
            page = pages[page_number]
        except IndexError as not_found:
            raise pyfe.PageNotFound(
                f"There is no page number {page_number}"
            ) from not_found

        return page, pages

    def __on_page(self, page_object, page) -> bool:
        """
        Is the page object ``page_object`` on page ``page`` ?

        :rtype: bool
        """

        # We just use numeric values of the Dim objects
        def corner_values(corner: list[dimensions.Dim]) -> list[float]:
            return [dim.value for dim in corner]

        # Get the top-left and bottom-right corners coordinates of the page

        if self.__is_model_page(page):
            page_tl = corner_values(page.box.coords["top-left"])
            page_br = corner_values(page.box.coords["bottom-right"])
        else:
            page_tl = corner_values(page.object.box.coords["top-left"])
            page_br = corner_values(page.object.box.coords["bottom-right"])

        # Get the top-left and bottom-right corners coordinates of the object

        if self.__is_model_object(page_object):
            object_tl = corner_values(page_object.box.coords["top-left"])
            object_br = corner_values(page_object.box.coords["bottom-right"])
        else:
            object_tl = corner_values(
                page_object.object.box.coords["top-left"]
            )
            object_br = corner_values(
                page_object.object.box.coords["bottom-right"]
            )

        # Object in X of the page
        in_x = object_tl[0] >= page_tl[0] and object_br[0] <= page_br[0]
        # Object in Y of the page
        in_y = object_tl[1] >= page_tl[1] and object_br[1] <= page_br[1]
        # Object in the page
        on_page = in_x and in_y

        return on_page

    def __overlaps_page(
        self, page_object, page, strict: bool = False
    ) -> Union[bool, dict]:
        """
        Is the page object ``page_object`` overlaping on page ``page`` ?

        :param page_object: Page object
        :param page: Page.
        :type strict: bool
        :param strict: Filter out page object that are totally contained in
            the page.

        :rtype: Union[bool, dict]
        """

        # We just use numeric values of the Dim objects
        def corner_values(corner: list[dimensions.Dim]) -> list[float]:
            return [dim.value for dim in corner]

        if self.__is_model_object(page_object):
            obj = {
                corner_name: corner_values(corner_coords)
                for corner_name, corner_coords in page_object.box.coords.items()
            }
        else:
            obj = {
                corner_name: corner_values(corner_coords)
                for corner_name, corner_coords in page_object.object.box.coords.items()
            }

        if self.__is_model_page(page):
            page_tl = corner_values(page.box.coords["top-left"])
            page_br = corner_values(page.box.coords["bottom-right"])
        else:
            page_tl = corner_values(page.object.box.coords["top-left"])
            page_br = corner_values(page.object.box.coords["bottom-right"])

        overlaping = {
            "top-left": False,
            "top-right": False,
            "bottom-left": False,
            "bottom-right": False,
        }

        for corner in ["top-left", "top-right", "bottom-left", "bottom-right"]:
            # Corner in X of the page
            in_x = (
                obj[corner][0] >= page_tl[0] and obj[corner][0] <= page_br[0]
            )
            # Corner in Y of the page
            in_y = (
                obj[corner][1] >= page_tl[1] and obj[corner][1] <= page_br[1]
            )
            # Corner in the page
            overlaping[corner] = in_x and in_y

        corner_overlaping = len(
            [status for status in overlaping.values() if status]
        )

        if corner_overlaping == 0:
            return False

        # If we want to know if the page object is overlaping on page but not
        # totally contained in that page, then having the four corners
        # overlaping should return False.
        if strict and corner_overlaping == 4:
            return False

        return overlaping

    def __is_model_page(self, page: Any) -> bool:
        """
        Is the object ``page`` an instance of model's page's classes ?
        page objects?

        :type page: Any
        :param page: Instance of page.
        :rtype: bool
        """
        return not isinstance(page, document.PageAbstract)

    def __is_model_object(self, page_object: Any) -> bool:
        """
        Is the object ``page_object`` an object from the list of the model
        page objects?

        :type page_object: Any
        :param page_object: A page object.
        :rtype: bool
        """

        for object_class in pageobjects.po_type_classes.values():
            if not isinstance(page_object, object_class):
                continue

            return True

        return False

    def __is_pyf_object(self, page_object: Any) -> bool:
        """
        Is the object ``page_object`` an object from the list of the interfaces
        to page objects?

        :type page_object: Any
        :param page_object: A page object.
        :rtype: bool
        """

        for object_class in po_interfaces.values():
            if not isinstance(page_object, object_class):
                continue

            return True

        return False

    def __transitional_class(self, page_object: Any) -> Any:
        """
        Transitional method: if the interface to ``page_object`` type is
        implemented, return an interface to this page object, else just
        returns ``page_object``.
        """

        if page_object.ptype in po_interfaces:
            interface_class = po_interfaces[page_object.ptype]

            return interface_class(
                self.file,
                model_object=page_object,
                converter=self.file.pica_converter,
            )

        return page_object

    def new_text(self, **kwargs) -> TextObject:
        """
        Create a new text object.

        :rtype: TextObject
        """

        text_frame = TextObject(
            self.file, new=True, converter=self.file.pica_converter, **kwargs
        )

        return text_frame

    def new_image(self, **kwargs) -> ImageObject:
        """
        Create a new image object.

        :rtype: ImageObject
        """

        image_frame = ImageObject(
            self.file, new=True, converter=self.file.pica_converter, **kwargs
        )

        return image_frame

    def new_polygon(self, **kwargs) -> PolygonObject:
        """
        Create a new image object.

        :rtype: PolygonObject
        """

        image_frame = PolygonObject(
            self.file, new=True, converter=self.file.pica_converter, **kwargs
        )

        return image_frame

    def append(self, page_object) -> bool:
        """
        Append the page object ``page_object``.

        .. warning:: This method only append page objects of the model
             classes. It does not do anything else and will change it's
             arguments and behaviour across versions.

        :rtype: bool
        :returns: Success.
        """

        is_model_object = self.__is_model_object(page_object)
        is_pyf_object = self.__is_pyf_object(page_object)

        if is_model_object:
            self.model.document.page_objects.append(page_object)
            return True

        if is_pyf_object:
            self.model.document.page_objects.append(page_object.object)
            return True

        return False

    def on_page(
        self,
        page_object,
        page_number: int,
        master: bool = False,
    ) -> bool:
        """
        Is the page object ``page_object`` on page number ``page_number`` ?

        .. note:: This methods detects objects strictely *contained* on pages
             — not objects overlaping in/out of pages. To detect overlaping
             objects, use ``overlaps_page`` method instead.

        :param page_object: Page object
        :type page_number: int
        :param page_number: Page number of the page to copy.
        :type master: bool
        :param master: Use the master pages set instead of normal pages.
        :rtype: bool
        :returns: ``True`` if the object is on the page.
        """

        # Get the relevant page
        page, _ = self.__get_page(page_number, master)

        return self.__on_page(page_object, page)

    def overlaps_page(
        self,
        page_object,
        page_number: int,
        master: bool = False,
        strict: bool = False,
        corners: bool = False,
    ) -> Union[bool, dict]:
        """
        Do the page object ``page_object`` overlaps on page number
        ``page_number`` ?

        .. warning:: This method checks only the non-rotated box of the page
            object.

        :param page_object: Page object
        :type page_number: int
        :param page_number: Page number.
        :type master: bool
        :param master: Use the master pages set instead of normal pages.
        :type strict: bool
        :param strict: Filter out page object that are totally contained in
            the page.
        :type corners: bool
        :param corners: Return the overlaping status of each corner of the page
            object. Returns a dictionary if there is overlaping corners, if
            not, returns ``False``.

        :rtype: Union[bool, dict]
        """
        page, _ = self.__get_page(page_number, master)

        overlaping = self.__overlaps_page(page_object, page, strict)

        if overlaping:
            if corners:
                return overlaping

            return True

        return False

    def __filter_name(self, lookup_set: list, pattern: StringOrNone) -> list:
        """
        Sub-filter for ``PageObjects.filter()``.

        Filter out page object not matching ``pattern``.

        Use fnmatch to match ``pattern`` like a file glob.

        :type lookup_set: list
        :param lookup_set: List of page objects to filter against.
        :type pattern: str
        :param pattern: Name pattern (glob) to match.
        :rtype: list
        """

        if pattern is None:
            return lookup_set

        return [
            page_object
            for page_object in lookup_set
            if fnmatch.fnmatchcase(page_object.name, pattern)
        ]

    def __filter_id(self, lookup_set: list, object_id: StringOrNone) -> list:
        """
        Sub-filter for ``PageObjects.filter()``.

        Filter out page object ID not matching ``object_id``.

        :type lookup_set: list
        :param lookup_set: List of page objects to filter against.
        :type object_id: str
        :param object_id: Object ID to match.
        :rtype: list
        """

        if object_id is None:
            return lookup_set

        return [
            page_object
            for page_object in lookup_set
            if page_object.object_id == object_id
        ]

    def __filter_page(
        self,
        lookup_set: list,
        page_number: Union[int, None],
        master: Union[bool, None] = False,
    ) -> list:
        """
        Sub-filter for ``PageObjects.filter()``.

        Filter out page object not in the page number ``page_number``.

        :type lookup_set: list
        :param lookup_set: List of page objects to filter against.
        :type page_number: int
        :param page_number: Page number.
        :type master: bool
        :param master: Use the master pages set instead of normal pages.
        :rtype: list
        """

        if page_number is None:
            return lookup_set

        # If master is None, then the user did not specified master
        # in PageObjects.filter, so we can assume the user want regular pages.
        if master is None:
            master = False

        page, _ = self.__get_page(page_number, master)

        filtered_set = [
            page_object
            for page_object in lookup_set
            if self.__on_page(page_object, page)
        ]

        return filtered_set

    def __filter_page_overlap(
        self,
        lookup_set: list,
        page_number: Union[int, None],
        master: Union[bool, None] = False,
        strict: bool = False,
    ) -> list:
        """
        Sub-filter for ``PageObjects.filter()``.

        Filter out page object not overlaping on the page number ``page_number``.

        :type lookup_set: list
        :param lookup_set: List of page objects to filter against.
        :type page_number: int
        :param page_number: Page number.
        :type master: bool
        :param master: Use the master pages set instead of normal pages.
        :type strict: bool
        :param strict: Filter out page object that are totally contained in
            the page.
        :rtype: list
        """

        if page_number is None:
            return lookup_set

        # If master_page is None, then the user did not specified master_page
        # in PageObjects.filter, so we can assume the user want regular pages.
        if master is None:
            master = False

        page, _ = self.__get_page(page_number, master)

        filtered_set = [
            page_object
            for page_object in lookup_set
            if self.__overlaps_page(page_object, page, strict)
        ]

        return filtered_set

    def __filter_type(
        self, lookup_set: list, object_type: Union[str, bool, None]
    ) -> list:
        """
        Sub-filter for ``PageObjects.filter()``.

        Filter by object type.

        :type lookup_set: list
        :param lookup_set: List of page objects to filter against.
        :type object_type: Union[str, bool, None]
        :param object_type: Page object type to filter, or do not filter at
            all. See pageobjects.po_type_classes for valid values.
        :rtype: list
        """
        if object_type is None:
            return lookup_set

        if not object_type:
            return lookup_set

        if object_type not in pageobjects.po_type_classes:
            raise ValueError(
                f"Wrong object type when filtering page objects: {object_type}"
            )

        filtered_set = [
            page_object
            for page_object in lookup_set
            if isinstance(
                page_object, pageobjects.po_type_classes[object_type]
            )
        ]

        return filtered_set

    def __filter_templatable(
        self, lookup_set: list, templatable: Union[bool, None] = False
    ) -> list:
        """
        Sub-filter for ``PageObjects.filter()``.

        Filter out non-templatable page objects.

        :type lookup_set: list
        :param lookup_set: List of page objects to filter against.
        :type templatable: Union[bool, None]
        :param templatable: Only return templatable page objects.
        :rtype: list
        """
        if templatable is None:
            return lookup_set

        if not templatable:
            return lookup_set

        if not self.file.templating["active"]:
            return []

        filtered_set = []

        for object_item in lookup_set:
            # If the page object is a text frame with templatable
            # stories, we add these templatable stories

            if isinstance(object_item, pageobjects.TextObject):
                po_templatable_stories = object_item.templatable()

                if po_templatable_stories:
                    filtered_set.extend(po_templatable_stories)

            else:
                # TODO If this page object is another type of page
                # object, we look its properties and find if it
                # is templatable through sla.SLA.templating settings

                if object_item.templatable():
                    filtered_set.append(object_item)

        return filtered_set

    @pyfe.has_document
    def filter(self, **kwargs) -> list:
        """
        Return document page objects.

        :rtype: list
        :returns: Filtered document page objects.

        .. seealso:: ``pyscribus.model.pageobjects.po_type_classes`` for valid
            values expected by ``object_type`` filter.

        +---------------------+-------------------------------------------+------+
        | Kwarg key           | Filter on                                 | Type |
        +=====================+===========================================+======+
        | object_type         | Object type                               | str  |
        +---------------------+-------------------------------------------+------+
        | object_id           | Object internal ID                        | str  |
        +---------------------+-------------------------------------------+------+
        | name                | Name of the page object. Glob-like and    | str  |
        |                     | case sensitive.                           |      |
        +---------------------+-------------------------------------------+------+
        | templatable         | Templatable objects                       | bool |
        +---------------------+-------------------------------------------+------+
        | page                | Objects in page X                         | int  |
        +---------------------+-------------------------------------------+------+
        | page_overlap        | Objects overlaping in page X.             | int  |
        |                     |                                           |      |
        |                     | Objects totally contained in page X are   |      |
        |                     | also returned.                            |      |
        +---------------------+-------------------------------------------+------+
        | page_overlap_strict | Objects overlaping in page X.             | int  |
        |                     |                                           |      |
        |                     | Objects totally contained in page X are   |      |
        |                     | NOT returned.                             |      |
        +---------------------+-------------------------------------------+------+
        | master_page         | When using the filters ``page`` and       | bool |
        |                     | ``page_overlap``, filter on master pages  |      |
        +---------------------+-------------------------------------------+------+

        :Example:

        .. code:: python

           # To avoid writing "scribus_file.pageobjects" again and again
           doc_objects = scribus_file.pageobjects

           templatable_text_frames = doc_objects.filter(
              object_type="text", templatable=True
           )

           on_page_two = doc_objects.filter(page=1)

           # Returns all the objects named after daleks, so we can EXTERMINATE
           # them instead of us humans.
           daleks = doc_objects.filter(name="Dalek *")

        """

        # Filter on page object ID ---------------------------------------

        filtered_set = self.__filter_id(self.items, kwargs.get("object_id"))

        # Filter on page object name -------------------------------------

        filtered_set = self.__filter_name(filtered_set, kwargs.get("name"))

        # Filter on page object type -------------------------------------

        filtered_set = self.__filter_type(
            filtered_set, kwargs.get("object_type")
        )

        # Filter on page -------------------------------------------------

        filtered_set = self.__filter_page(
            filtered_set, kwargs.get("page"), kwargs.get("master_page")
        )

        # Filter on overlaping pages -------------------------------------

        filtered_set = self.__filter_page_overlap(
            filtered_set,
            kwargs.get("page_overlap"),
            kwargs.get("master_page"),
            False,
        )

        # Filter on overlaping pages (strict) ----------------------------

        filtered_set = self.__filter_page_overlap(
            filtered_set,
            kwargs.get("page_overlap_strict"),
            kwargs.get("master_page"),
            True,
        )

        # Filter off templatable page objects ----------------------------

        filtered_set = self.__filter_templatable(
            filtered_set, kwargs.get("templatable")
        )

        # ----------------------------------------------------------------

        return [self.__transitional_class(object) for object in filtered_set]

    @property
    @pyfe.has_document
    def items(self) -> list:
        """List of all page objects."""
        return self.model.document.page_objects

    def __iter__(self):
        for page_object in self.model.document.page_objects:
            yield page_object

    def __getitem__(self, item: int):
        return self.model.document.page_objects[item]


# vim:set shiftwidth=4 softtabstop=4:
