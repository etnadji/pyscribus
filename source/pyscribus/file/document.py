#!/usr/bin/python3
# -*- coding:Utf-8 -*-

# PyScribus, python library for Scribus SLA
# Copyright (C) 2020-2023 Étienne Nadji
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""
PyScribus

Scribus file. Document classes.
"""

# Imports ===============================================================#

# Standard library ---------------------------------------------

# To avoid Sphinx complaints from methods annotations referencing same class
from __future__ import annotations

import copy

from typing import Any, Union, Optional, Literal, NoReturn
from collections import OrderedDict

# PyScribus model ----------------------------------------------

import pyscribus.model.pages as pages
import pyscribus.model.layers as layers
import pyscribus.model.dimensions as dimensions
import pyscribus.model.pageobjects as pageobjects

from pyscribus.model.sla import SLA

# PyScribus file -----------------------------------------------

import pyscribus.file as pyf
import pyscribus.file.exceptions as pyfe

# Global variables / Annotations ========================================#

__author__ = "Etienne Nadji <etnadji@eml.cc>"

FirstLast = Literal["first", "last"]
StringOrNone = Union[str, None]

# =======================================================================#


class PageAbstract:
    """
    Abstract class for interfaces to Page, MasterPage model classes.
    """

    def __init__(self, file, **kwargs):
        self.file = file
        self.object = None
        self._converter = None

        if (converter := kwargs.get("converter")) is not None:
            self._converter = converter

        if (new := kwargs.get("new")) is not None:
            if bool(new):
                self._new(**kwargs)

        if (model_object := kwargs.get("model_object")) is not None:
            self.object = model_object

    def _picas(self, value: Union[int, float]) -> float:
        if self._converter is None:
            return value

        return self._converter.picas(value)

    def _unit(self, value: Union[int, float]) -> float:
        if self._converter is None:
            return value

        return self._converter.unit(value)

    def _picas_coord(self, axis, value: Union[int, float]) -> float:
        if self._converter is None:
            return value

        return self._converter.picas_coord(axis, value)

    def _unit_coord(self, axis, value: Union[int, float]) -> float:
        if self._converter is None:
            return value

        return self._converter.unit_coord(axis, value)

    def _new(self, **kwargs) -> NoReturn:
        """
        Initialising page object from scratch.
        """
        self._new_model(**kwargs)
        # TODO insert page number here
        self._new_settings(**kwargs)

    def _new_settings(self, **kwargs) -> NoReturn:
        """
        Handle kwargs values to initialize values that doesn't need to
        be initialized with the model in ``_new_model`` method, for example,
        with class properties instead of altering values in the underlying
        model.

        In subclasses, refering to this method when coding ``_new_settings``
        again is mandatory.
        """

        for setting_name, setting_value in kwargs.items():
            if setting_name == "pos_x":
                self.pos_x = setting_value
            if setting_name == "pos_y":
                self.pos_y = setting_value
            if setting_name == "height":
                self.height = setting_value
            if setting_name == "width":
                self.width = setting_value

    def _new_model(self, **kwargs) -> NoReturn:
        """
        Placeholder method for subclasses.

        Create the underlying page model here and attribute it to ``object``
        attribute.
        """

    # Coordinates --------------------------------------------------------

    @property
    @pyfe.has_object_model
    def pos_x(self) -> float:
        """Page's X coordinate of the top-level corner."""
        return self._unit_coord("x", self.object.box.getx("top-left"))

    @pos_x.setter
    @pyfe.has_object_model
    def pos_x(self, value: float) -> NoReturn:
        self.object.box.setx("top-left", self._picas_coord("x", value))

    @property
    @pyfe.has_object_model
    def pos_y(self) -> float:
        """Page's Y coordinate of the top-level corner."""
        return self._unit_coord("y", self.object.box.gety("top-left"))

    @pos_y.setter
    @pyfe.has_object_model
    def pos_y(self, value: float) -> NoReturn:
        self.object.box.sety("top-left", self._picas_coord("y", value))

    # Size ---------------------------------------------------------------

    @property
    @pyfe.has_object_model
    def width(self) -> float:
        """Page object width."""
        return self._unit(self.object.box.dims["width"].value)

    @width.setter
    @pyfe.has_object_model
    def width(self, value: float) -> NoReturn:
        self.object.box.dims["width"].value = self._picas(value)

    @property
    @pyfe.has_object_model
    def height(self) -> float:
        """Page object height."""
        return self._unit(self.object.box.dims["height"].value)

    @height.setter
    @pyfe.has_object_model
    def height(self, value: float) -> NoReturn:
        self.object.box.dims["height"].value = self._picas(value)

    # Left position ? ----------------------------------------------------

    @property
    @pyfe.has_object_model
    def left(self) -> bool:
        """On multipage spreads, is this page on the left?"""
        return self.object.is_leftest

    # Page number --------------------------------------------------------

    @property
    @pyfe.has_object_model
    def number(self) -> int:
        """
        Page's number (setter).

        .. note:: This only sets the page's number. It doesn't move objects
            in the document according to that change. Use ``Pages`` methods
            for that.

        """
        return self.object.number

    @number.setter
    @pyfe.has_object_model
    def number(self, value: int) -> NoReturn:
        """Page's number (setter)."""
        self.object.number = value

    # Orientation --------------------------------------------------------

    @property
    @pyfe.has_object_model
    def orientation(self) -> str:
        return self.object.orientation

    @orientation.setter
    @pyfe.has_object_model
    def orientation(self, value: Literal["portrait", "landscape"]) -> str:
        try:
            value = value.lower()
        except ValueError as not_string:
            raise ValueError(
                "(Master)Page's possible orientation are "
                f"'portrait', 'orientation'. Got '{value}'."
            ) from not_string
        except AttributeError as no_lower:
            raise ValueError(
                "(Master)Page's possible orientation are "
                f"'portrait', 'orientation'. Got '{value}'."
            ) from no_lower

        if value not in ["portrait", "landscape"]:
            raise ValueError(
                "(Master)Page's possible orientation are "
                f"'portrait', 'orientation'. Got '{value}'."
            )

        self.object.orientation = value

    # Guides -------------------------------------------------------------

    @property
    @pyfe.has_object_model
    def horizontal_guides(self) -> list[float]:
        """Horizontal guides (positions related to this page)."""
        return [
            self._unit(guide.value)
            for guide in self.object.guides["horizontal"]
        ]

    @property
    @pyfe.has_object_model
    def vertical_guides(self) -> list[float]:
        """Vertical guides (positions related to this page)."""
        return [
            self._unit(guide.value) for guide in self.object.guides["vertical"]
        ]

    @property
    @pyfe.has_object_model
    def abs_horizontal_guides(self) -> list[float]:
        """Horizontal guides (absolute positions)."""
        return [
            self._unit(guide.value) + self.pos_y
            for guide in self.object.guides["horizontal"]
        ]

    @property
    @pyfe.has_object_model
    def abs_vertical_guides(self) -> list[float]:
        """Vertical guides (absolute positions)."""
        return [
            self._unit(guide.value) + self.pos_x
            for guide in self.object.guides["vertical"]
        ]

    def abs_guide(
        self, guide: float, guide_type: Literal["horizontal", "vertical"]
    ) -> float:
        """
        Returns the absolute position of a page guide (instead of it's
        position relative to the page).

        If you want to iterate over absolute guides, use the
        ``abs_horizontal_guides`` and ``abs_vertical_guides`` properties.

        :type guide: float
        :param guide: Position of the guide relatively to its page.
        :type guide_type: Literal["horizontal", "vertical"]
        :param guide_type: Type of guide.
        :rtype: float
        :returns: Absolute position of the guide, in defined unit or picas.
        """

        if guide_type not in ["horizontal", "vertical"]:
            raise ValueError(
                "Possible guide type values: 'horizontal', 'vertical'. "
                f"Got '{guide_type}'"
            )

        if guide_type == "horizontal":
            offset = self.pos_y

        if guide_type == "vertical":
            offset = self.pos_x

        return guide + offset

    # Borders ------------------------------------------------------------

    @property
    @pyfe.has_object_model
    def border_right(self) -> float:
        """
        Page's right border (in picas).
        """
        return self._unit(self.object.borders["right"].value)

    @border_right.setter
    @pyfe.has_object_model
    def border_right(self, value: float) -> NoReturn:
        """Page's right border (setter)."""
        self.object.borders["right"].value = self._picas(value)

    @property
    @pyfe.has_object_model
    def border_left(self) -> float:
        """
        Page's left border (in picas).
        """
        return self._unit(self.object.borders["left"].value)

    @border_left.setter
    @pyfe.has_object_model
    def border_left(self, value: float) -> NoReturn:
        """Page's left border (setter)."""
        self.object.borders["left"].value = self._picas(value)

    @property
    @pyfe.has_object_model
    def border_top(self) -> float:
        """
        Page's top border (in picas).
        """
        return self._unit(self.object.borders["top"].value)

    @border_top.setter
    @pyfe.has_object_model
    def border_top(self, value: float) -> NoReturn:
        """Page's top border (setter)."""
        self.object.borders["top"].value = self._picas(value)

    @property
    @pyfe.has_object_model
    def border_bottom(self) -> float:
        """
        Page's bottom border (in picas).
        """
        return self._unit(self.object.borders["bottom"].value)

    @border_bottom.setter
    @pyfe.has_object_model
    def border_bottom(self, value: float) -> NoReturn:
        """Page's border (setter)."""
        self.object.borders["bottom"].value = self._picas(value)


class Page(PageAbstract):
    """
    Interface to a Scribus page.
    """

    # NOTE. A regular page can't have a name, as setting its name make
    # Scribus crash.

    def _new_model(self, **kwargs) -> NoReturn:
        self.object = pages.Page()

    @property
    @pyfe.has_object_model
    def master_name(self) -> str:
        """Name of the master page applied."""
        return self.object.master_name

    @master_name.setter
    @pyfe.has_object_model
    def master_name(self, value: str) -> NoReturn:
        """Name of the master page applied (setter)."""
        self.object.name = value


class MasterPage(PageAbstract):
    """
    Interface to a Scribus master page.
    """

    def _new_model(self, **kwargs) -> NoReturn:
        self.object = pages.MasterPage()

    @property
    @pyfe.has_object_model
    def name(self) -> str:
        """Name of that (master)page."""
        return self.object.name

    @name.setter
    @pyfe.has_object_model
    def name(self, value: str) -> NoReturn:
        """Name of that (master)page (setter)."""
        self.object.name = value


# =======================================================================#


class Metadatas:
    """
    Interface for document's metadatas.

    :Example:

    .. code:: python

       from pyscribus.file import ScribusFile

       doc = ScribusFile("1.5.5", "example.sla")

       title = doc.metadatas.title
       date = doc.metadatas.date

       doc.metadatas.add_keyword("NewKeyword")

       metadatas_dump = dict(doc.metadatas)

    """

    def __init__(self, model: SLA):
        self.model = model

    @pyfe.has_document
    def __get(self, key) -> Union[str, list[str], None]:
        return self.model.document.metadata.get(key)

    def __iter__(self):
        """
        Special method to make Metadatas class iterable.
        """
        for key, value in self.model.document.metadata.items():
            yield (key, value)

    # Keywords -----------------------------------------------------------

    @property
    def keywords(self) -> Union[list[str], None]:
        return self.__get("keywords")

    def add_keyword(self, new_keyword: str) -> NoReturn:
        """
        Add a new keyword to the document's metadatas.

        :type new_keyword: str
        :param new_keyword: New keyword to add.
        """

        keywords = self.__get("keywords")

        if keywords is None:
            keywords = []
        else:
            keywords = list(set(keywords))

        if new_keyword not in keywords:
            keywords.append(new_keyword)

        self.model.document.metadata["keywords"] = keywords

    # --------------------------------------------------------------------

    @property
    def title(self) -> StringOrNone:
        return self.__get("title")

    @property
    def author(self) -> StringOrNone:
        """Author(s) of the document"""
        return self.__get("author")

    @property
    def subject(self) -> StringOrNone:
        return self.__get("subject")

    @property
    def comments(self) -> StringOrNone:
        return self.__get("comments")

    @property
    def publisher(self) -> StringOrNone:
        return self.__get("publisher")

    @property
    def contributor(self) -> StringOrNone:
        """Contributor(s) to the document"""
        return self.__get("contributor")

    @property
    def date(self) -> StringOrNone:
        return self.__get("date")

    @property
    def type(self) -> StringOrNone:
        return self.__get("type")

    @property
    def format(self) -> StringOrNone:
        return self.__get("format")

    @property
    def identifier(self) -> StringOrNone:
        return self.__get("identifier")

    @property
    def source(self) -> StringOrNone:
        return self.__get("source")

    @property
    def lang(self) -> StringOrNone:
        return self.__get("lang")

    @property
    def related(self) -> StringOrNone:
        return self.__get("related")

    @property
    def cover(self) -> StringOrNone:
        return self.__get("cover")

    @property
    def rights(self) -> StringOrNone:
        return self.__get("rights")


class Pages:
    """
    Interface for document's pages.

    :Example:

    .. code:: python

       from pyscribus.file import ScribusFile
       from pyscribus.model.pages import Page

       doc = ScribusFile("1.5.5", "example.sla")

       doc_pages = doc.pages

       # Equivalent to doc.pages[0]
       page_one = doc_pages.pages[0]
       how_many_pages = len(doc_pages)
       first_page_copy = doc_pages.copy(0)

       # Master pages are in a attribute of Pages
       master_pages = doc_pages.master_pages
       how_many_master_pages = len(doc_pages.master_pages)

       # BELOW THIS LINE, EVERYTHING IS NOT IMPLEMENTED --------

       doc_pages.remove(5)
       new_page = Page()
       doc_pages.insert(2, new_page)

    """

    def __init__(self, file: pyf.ScribusFile):
        self.file = file
        self.model = file.model

    def __len__(self) -> int:
        # > len(x.pages)
        return len(self.__pages_number_sorted("normal"))

    def __getitem__(self, item: int):
        # > x.pages[0]
        # return self.pages[item]
        return list(self.__pages_number_sorted("normal").values())[item]

    @pyfe.has_document
    def __pages_number_sorted(self, page_set="normal") -> OrderedDict:
        if page_set == "normal":
            p_set = self.model.document.pages
        elif page_set == "master":
            p_set = self.model.document.master_pages
        else:
            raise ValueError(f"Asked for invalid page set {page_set}")

        ordered_pages = OrderedDict()
        numbers = sorted([page.number for page in p_set])

        for number in numbers:
            for page in p_set:
                if page.number != number:
                    continue

                if page_set == "normal":
                    ordered_pages[number] = Page(
                        self.file,
                        model_object=page,
                        converter=self.file.pica_converter,
                    )
                if page_set == "master":
                    ordered_pages[number] = MasterPage(
                        self.file,
                        model_object=page,
                        converter=self.file.pica_converter,
                    )

                break

        return ordered_pages

    @pyfe.has_document
    def insert(self, position: Union[int, FirstLast], page: Any):
        """
        .. warning:: Not implemented.

        Insert page at page_number ``position``.

        :type position: Union[int, FirstLast]
        :param position: Page number where to insert the page.
        """
        # > x.pages.insert(2, new_page)

        len_pages = len(self)

        insert_last = position == "last" or position == len_pages
        insert_first = position == "first" or position == 0

        if insert_first:
            pass
        if insert_last:
            pass
        else:
            pass

        raise NotImplementedError()

    def copy(
        self,
        position: int,
        new_page_number: Optional[int] = None,
        master_page: bool = False,
    ) -> Union[Page, MasterPage]:
        """
        Returns a copy of the (master)page at ``position`` page_number.

        .. note:: It does not copy page objects, nor does it add the returned
            page to the document.

        :type position: int
        :param position: Page number of the page to copy.
        :type new_page_number: Optional[int]
        :param new_page_number: New page number to attribute to the copy.
        :type master_page: bool
        :param master_page: Copy a master page instead of a regular page.
        :rtype: Union[Page, MasterPage]
        :returns: Copy of the (master)page.
        """
        page_set = {False: "normal", True: "master"}[bool(master_page)]

        try:
            original = list(self.__pages_number_sorted(page_set).values())[
                position
            ]
        except IndexError as not_found:
            raise pyfe.PageNotFound(
                f"There is no page number {position}"
            ) from not_found

        page_copy = copy.deepcopy(original)

        if new_page_number is not None:
            page_copy.number = new_page_number

        return page_copy

    @pyfe.has_document
    def remove(self, page_number: int):
        """
        .. warning:: Not implemented.

        """
        # > x.pages.remove(5)
        # How to :
        # - Catch the coords of the page to delete
        # - Catch the coords of the pages following page_number index
        # - Attribute coords of page_number + 1 the position of page_number + 0
        # - Modify page_number attribute of page_number + 1
        # -   Etc.
        raise NotImplementedError()

    @property
    def pages(self) -> list[Page]:
        # > x.pages
        # return self.model.document.pages
        return list(self.__pages_number_sorted("normal").values())

    @property
    def masterpages(self) -> list[MasterPage]:
        # return self.model.document.master_pages
        return list(self.__pages_number_sorted("master").values())


class Fonts:
    """
    .. warning:: Not implemented.

    Interface for document's fonts.
    """

    # This should inspect styles, Story elements to produce a list/dict of
    # used fonts and their use context.

    def __init__(self, model):
        self.model = model
        self.document = model.document

    def __getitem__(self, item):
        return self.__fonts()[item]

    @pyfe.has_document
    def __fonts(self) -> dict:
        pass


class Layers:
    """
    .. warning:: Not implemented.

    Interface for document's layers.
    """

    def __init__(self, model):
        self.model = model

    def __getitem__(self, item):
        return self.__layers()[item]

    def __iter__(self):
        """
        Special method to make Layers class iterable.
        """

        for layer in self.model.document.layers:
            yield layer

    @pyfe.has_document
    def __layers(self) -> dict:
        return self.model.document.layers

    def insert(self, layer: str, after: int, before: int):
        """
        .. warning:: Not implemented.

        Move layer.
        """


# vim:set shiftwidth=4 softtabstop=4 spl=en:
