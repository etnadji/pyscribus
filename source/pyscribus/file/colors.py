#!/usr/bin/python3
# -*- coding:Utf-8 -*-

# PyScribus, python library for Scribus SLA
# Copyright (C) 2020-2023 Étienne Nadji
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""
PyScribus

Scribus file. Colors and gradients wrappers.
"""

# Imports ===============================================================#

# Standard library ---------------------------------------------

# To avoid Sphinx complaints from methods annotations referencing same class
from __future__ import annotations

from typing import NoReturn, Union, Literal

# PyScribus model ----------------------------------------------

from pyscribus.model.sla import SLA
import pyscribus.model.colors as colors

# PyScribus file -----------------------------------------------

import pyscribus.file as pyf
import pyscribus.file.exceptions as pyfe

# Variables globales ====================================================#

__author__ = "Etienne Nadji <etnadji@eml.cc>"

ColorSpace = Literal["cmyk", "rgb"]

# Classes ===============================================================#


class Color:
    """
    Interface class for Scribus colors.
    """

    def __init__(self, file, **kwargs):
        self.file = file
        self.object = None

        if (new := kwargs.get("new")) is not None:
            if bool(new):
                self._new(**kwargs)

        if (model_object := kwargs.get("model_object")) is not None:
            self.object = model_object

    def _new(self, **kwargs) -> NoReturn:
        """
        Initialising color from scratch.
        """
        self._new_model(**kwargs)
        self._new_settings(**kwargs)

    def _new_model(self, **kwargs) -> NoReturn:
        self.object = colors.Color()

    def _new_settings(self, **kwargs) -> NoReturn:
        """
        Handle settings at new color creation.
        """

        # Color space is treated first. ------------------------------

        space_setting = kwargs.get("space")

        if space_setting is not None:
            self.space = space_setting

        # Other settings ---------------------------------------------

        for setting_name, setting_value in kwargs.items():
            if setting_name == "space":
                continue

            if setting_name == "name":
                self.name = setting_value

            if setting_name == "rgb":
                self.rgb = setting_value

            if setting_name == "cmyk":
                self.cmyk = setting_value

    # Special methods ----------------------------------------------------

    def __str__(self) -> str:
        return self.name

    def __eq__(self, other: Color) -> bool:
        # We use the special method __eq__ of the model's class Color.
        return self.object == other.object

    # Name ---------------------------------------------------------------

    @property
    @pyfe.has_object_model
    def name(self) -> str:
        """Color name."""
        return self.object.name

    @name.setter
    @pyfe.has_object_model
    def name(self, value: str) -> NoReturn:
        """Color name (setter)."""
        self.object.name = value

    # Inks (by color space) ----------------------------------------------

    def __missing_ink(self, inks: dict, ink_key: str) -> float:
        """
        Return 0 of a ink is missing from ``rgb``, ``cmyk`` properties setter
        argument.

        :rtype: float
        """
        ink_value = inks.get(ink_key)

        if ink_value is None:
            return 0.0

        return ink_value

    @property
    @pyfe.has_object_model
    def rgb(self) -> Union[dict, None]:
        """
        RGB inks of this color, or ``None``.

        .. note:: Setting this property change the color space to RGB.

        :rtype: Union[dict, None]
        """
        if self.object.is_cmyk:
            return None

        return self.object.colors

    @rgb.setter
    @pyfe.has_object_model
    def rgb(self, inks: dict) -> NoReturn:
        """RGB inks of this color (setter)."""

        self.object.set_space_colors(
            "cmyk",
            [
                self.__missing_ink(inks, ink_name)
                for ink_name in ["R", "G", "B"]
            ],
        )

    @property
    @pyfe.has_object_model
    def cmyk(self) -> Union[dict, None]:
        """
        CMYK inks of this color, or ``None``.

        .. note:: Setting this property change the color space to CMYK.

        :rtype: Union[dict, None]
        """
        if self.object.is_rgb:
            return None

        return self.object.colors

    @cmyk.setter
    @pyfe.has_object_model
    def cmyk(self, inks: dict) -> NoReturn:
        """CMYK inks of this color (setter)."""

        self.object.set_space_colors(
            "cmyk",
            [
                self.__missing_ink(inks, ink_name)
                for ink_name in ["C", "M", "Y", "K"]
            ],
        )

    # Inks (individual) --------------------------------------------------

    def __get_ink(self, space: ColorSpace, ink_name: str) -> float:
        if space != self.space:
            return None

        return self.object.colors[ink_name]

    def __set_ink(
        self, space: ColorSpace, ink_name: str, value: float
    ) -> bool:
        if space != self.space:
            return False

        if value < float(0.0):
            value = 0.0

        if space == "cmyk" and value > float(100.0):
            value = 100.0
        if space == "rgb" and value > float(255.0):
            value = 255.0

        self.object.colors[ink_name] = value

        return True

    @property
    @pyfe.has_object_model
    def R(self) -> Union[float, None]:
        """RGB space: red."""
        return self.__get_ink("rgb", "R")

    @R.setter
    @pyfe.has_object_model
    def R(self, value: float) -> float:
        """RGB space: red (setter)."""
        return self.__set_ink("rgb", "R", value)

    @property
    @pyfe.has_object_model
    def G(self) -> Union[float, None]:
        """RGB space: green."""
        return self.__get_ink("rgb", "G")

    @G.setter
    @pyfe.has_object_model
    def G(self, value: float) -> float:
        """RGB space: green (setter)."""
        return self.__set_ink("rgb", "G", value)

    @property
    @pyfe.has_object_model
    def B(self) -> Union[float, None]:
        """RGB space: blue."""
        return self.__get_ink("rgb", "B")

    @B.setter
    @pyfe.has_object_model
    def B(self, value: float) -> float:
        """RGB space: blue (setter)."""
        return self.__set_ink("rgb", "B", value)

    @property
    @pyfe.has_object_model
    def C(self) -> Union[float, None]:
        """CMYK space: cyan."""
        return self.__get_ink("cmyk", "C")

    @C.setter
    @pyfe.has_object_model
    def C(self, value: float) -> float:
        """CMYK space: cyan (setter)."""
        return self.__set_ink("cmyk", "C", value)

    @property
    @pyfe.has_object_model
    def M(self) -> Union[float, None]:
        """CMYK space: magenta."""
        return self.__get_ink("cmyk", "M")

    @M.setter
    @pyfe.has_object_model
    def M(self, value: float) -> float:
        """CMYK space: magenta (setter)."""
        return self.__set_ink("cmyk", "M", value)

    @property
    @pyfe.has_object_model
    def Y(self) -> Union[float, None]:
        """CMYK space: yellow."""
        return self.__get_ink("cmyk", "Y")

    @Y.setter
    @pyfe.has_object_model
    def Y(self, value: float) -> float:
        """CMYK space: yellow (setter)."""
        return self.__set_ink("cmyk", "Y", value)

    @property
    @pyfe.has_object_model
    def K(self) -> Union[float, None]:
        """CMYK space: black."""
        return self.__get_ink("cmyk", "K")

    @K.setter
    @pyfe.has_object_model
    def K(self, value: float) -> float:
        """CMYK space: black (setter)."""
        return self.__set_ink("cmyk", "K", value)

    # Color space --------------------------------------------------------

    @property
    @pyfe.has_object_model
    def space(self) -> Union[str, None]:
        """
        Color space of this color.

        .. note:: Changing this property will not change inks applied. To
            change space and inks, use the properties ``rgb`` or ``cmyk``
            instead.

        """
        return self.object.space

    @space.setter
    @pyfe.has_object_model
    def space(self, value: ColorSpace) -> NoReturn:
        self.object.set_space(value)


class Colors:
    """
    Wrapper around Scribus colors.

    :type file: pyscribus.file.ScribusFile
    :param file: Scribus file wrapper

    :Example:

    .. code:: python

       from pyscribus.file import ScribusFile

       doc = ScribusFile("1.5.5", "example.sla")

       how_many_colors = len(doc.colors)
       black_color = doc.colors["Black"]
       cmyk_colors = doc.colors.filter(space="cmyk")

    """

    def __init__(self, file: pyf.ScribusFile):
        self.file = file
        self.model = file.model

    def __len__(self) -> int:
        # > len(x.colors)
        return len(self.__colors())

    def __getitem__(self, item: int):
        # > x.colors["black"]
        return self.__colors()[item]

    @pyfe.has_document
    def __colors(self) -> dict:
        return {
            color.name: Color(self.file, model_object=color)
            for color in self.model.document.colors
        }

    @pyfe.has_document
    def filter(self, **kwargs) -> list[colors.Color]:
        """
        Returns a selection of colors.

        :type kwargs: dict
        :param kwargs: kwargs
        :rtype: list[Color]
        :returns: Selection of colors

        **Kwargs options :**

        +-----------------------+-------------+-------------------------+
        | Kwarg key             | Filter on   | Type                    |
        +=======================+=============+=========================+
        | space                 | Color space | ``"cmyk"`` or ``"rgb"`` |
        +-----------------------+-------------+-------------------------+
        """

        to_filter = self.__colors()

        filter_space = False

        if (space := kwargs.get("space")) is not None:
            if space.lower() in ["cmyk", "rgb"]:
                filter_space = space.lower()

        if filter_space:
            to_filter = {
                color.name: color
                for color in to_filter.values()
                if color.space == filter_space
            }

        return to_filter

    @property
    def items(self) -> dict:
        """
        Colors dictionary organized by color name.

        .. seealso:: :class:`pyscribus.model.colors.Color`

        :rtype: dict
        """
        return self.__colors()


# vim:set shiftwidth=4 softtabstop=4:
