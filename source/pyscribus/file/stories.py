#!/usr/bin/python3
# -*- coding:Utf-8 -*-

# PyScribus, python library for Scribus SLA
# Copyright (C) 2020-2023 Étienne Nadji
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""
PyScribus

Scribus file. Stories wrapper.
"""

# Imports ===============================================================#

# To avoid Sphinx complaints from methods annotations referencing same class
from __future__ import annotations

from typing import Union, Literal, NoReturn, Any

import copy

import pyscribus.file as pyf
import pyscribus.file.exceptions as pyfe

import pyscribus.model.notes as model_notes
import pyscribus.model.marks as model_marks
import pyscribus.model.stories as model_stories
# import pyscribus.model.pageobjects as model_pageobjects

# Variables globales ====================================================#

__author__ = "Etienne Nadji <etnadji@eml.cc>"

StoryFieldType = Literal["page_number", "page_count"]

# Classes ===============================================================#

# ---------------------------------------------------------------------------

class Note:
    """
    High level interface to (foot|end)note.
    """

    def __init__(self, parent_interface: Stories, **kwargs):
        # Stories interface
        self.parent = parent_interface
        # ScribusFile
        self.file = parent_interface.file
        # pyscribus.model.SLA
        self.model = parent_interface.file.model

        self.sequence = []

        # Content of the note --------------------------------------------

        self.__object_content = None

        # Frame showing (but not storing) the note -----------------------

        self.__object_frame = None

        # Page object & story storing the note ---------------------------

        # Mark of the note in the story
        self.__object_story_mark = None

        # Page object in which the story mark of the note is located
        self.__object_story_frame = None

        # References to marks and frames ---------------------------------

        # Reference of the note mark in global list of marks
        self.__object_ref_mark = None
        # Reference of the frame showing the note in the global list of
        # frames showing notes.
        self.__object_ref_frame = None

        # ----------------------------------------------------------------

        self._note_style = "Default"

        if (is_new := kwargs.get("new")) is not None:
            if is_new:
                self._new(**kwargs)

        if (seq := kwargs.get("sequence")) is not None:
            self.sequence = seq
            self.update()

        if (nmark := kwargs.get("story_mark")) is not None:
            self._load(nmark, **kwargs)

    @property
    def label(self):
        return self.__object_story_mark.label

    def _new(self, **kwargs):
        # Creating a new ID for the note ---------------------------------

        # NOTE
        # The possibility for different notes styled seems to be in the roadmap
        # of Scribus according to the SLA format, yet Scribus GUI allows only
        # the modification of *one* note styled called Default, which can't be
        # renamed.
        # If that changes, add a kwargs key note_style, which will be fed to
        # this Note._new method.
        note_style = "Default"

        # We get the new note count from a private counter in the Stories
        # interface.
        note_count = self.parent._increase_notes_count(note_style)

        # Note's labels are formatted this way by Scribus
        new_label = f"NoteMark_{note_style}_{note_count}"

        # Mark for the global list of marks ------------------------------

        self.__object_ref_mark = model_marks.DocumentMark()
        self.__object_ref_mark.set_type("note")
        self.__object_ref_mark.label = new_label

        # Mark in the story ----------------------------------------------

        self.__object_story_mark = model_marks.StoryNoteMark()
        self.__object_story_mark.fromdefault()
        self.__object_story_mark.label = new_label

        # Note content ---------------------------------------------------

        self.__object_content = model_notes.Note()
        self.__object_content.fromdefault()
        self.__object_content.style = new_label.split("_")[1]
        self.__object_content.parent_mark = new_label

        # ----------------------------------------------------------------

        doc = self.file.model.document

        if doc.notes_frames:
            for note_frame in doc.notes_frames:
                print(note_frame)
        else:
            self.__object_frame = model_notes.NoteFrame()
            self.__object_frame.own_frame_id = None
            self.__object_frame.note_style = note_style

        # print()
        # print("content:", self.__object_content.text)
        # print("frame:", self.__object_frame)
        # print("story mark:", self.__object_story_mark)
        # print("story frame:", self.__object_story_frame)
        # print("mark ref:", self.__object_ref_mark)
        # print("frame ref:", self.__object_ref_frame)
        # print()

    @pyfe.has_document
    def append(self, item: Union[Paragraph, Text, SpanText]) -> bool:
        """
        Append content to this note.

        :rtype: bool
        :returns: Success
        """
        def valid_item(sub_item) -> bool:
            for item_class in [Paragraph, Text, SpanText]:
                if isinstance(sub_item, item_class):
                    return True

            return False

        if isinstance(item, list):
            for element in item:
                if valid_item(element):
                    self.sequence.append(element)

            return True

        if not valid_item(item):
            return False

        self.sequence.append(item)

        return True

    @pyfe.has_document
    def _load(self, model, **kwargs):
        self.__object_story_mark = model

        # Get the note's content object
        #
        # => + object_content

        for note in self.model.document.notes:
            if note.parent_mark != model.label:
                continue

            self.__object_content = note
            break

        # Get the note's marks in the global list of marks
        #
        # => + object_ref_mark

        for mark in self.model.document.marks:
            if mark.label != model.label:
                continue

            self.__object_ref_mark = mark
            break

        # Get the ID of the frame containing the story in which the note mark
        # is located…
        #
        # => + object_story_frame

        story_frame_id = None

        for page_object in self.file.pageobjects.filter(type="text"):

            if self.__object_story_frame is not None:
                break

            if not page_object.story:
                continue

            if isinstance(page_object.story, model_stories.Story):
                sequence = page_object.story.sequence
            else:
                sequence = page_object.story.object.sequence

            for element in sequence:
                if not isinstance(element, model_marks.StoryNoteMark):
                    continue

                if element.label != model.label:
                    continue

                story_frame_id = page_object.object.object_id
                self.__object_story_frame = page_object
                break

        # … from which we get the ID of the text frame where the content of the
        # story is put (but not stored) from the list of notes frames…
        #
        # => + object_ref_frame

        for note_frame in self.model.document.notes_frames:
            if note_frame.story_frame_id != story_frame_id:
                continue

            self.__object_ref_frame = note_frame
            break

        # … from which we get the text frame where the content of the story is
        # put (but not stored)…
        #
        # => + object_frame

        for page_object in self.file.pageobjects.filter(type="text"):
            if page_object.object.object_id != self.__object_ref_frame.own_frame_id:
                continue

            self.__object_frame = page_object
            break

        # print()
        # print("content:", self.__object_content)
        # print("frame:", self.__object_frame)
        # print("story mark:", self.__object_story_mark)
        # print("story frame:", self.__object_story_frame)
        # print("mark ref:", self.__object_ref_mark)
        # print("frame ref:", self.__object_ref_frame)

        return True

    def update(self):
        new_sequence = self._compile()

    def _compile(self, context: Literal["story", "note", "mark", "frame"]) -> list:
        try:
            compile_method = {
                "frame": self._compile_frame,
                "story": self._compile_story,
                "note": self._compile_note,
                "mark": self._compile_mark,
            }[context]
        except KeyError:
            raise ValueError(
                f"Invalid context for Note compiling into model: {context}"
            )

        return compile_method()

    def _compile_frame(self) -> list:
        return self.__object_frame

    def _compile_mark(self) -> list:
        return self.__object_ref_mark

    def _compile_note(self) -> list:
        for item in self.sequence:
            if isinstance(item, Paragraph):
                self.__object_content.text.sequence += item._compile("note")
            if isinstance(item, Text):
                self.__object_content.text.sequence += item._compile("note")
            if isinstance(item, SpanText):
                self.__object_content.text.sequence += item._compile("note")

        return self.__object_content

    def _compile_story(self) -> list:
        # TODO Get numerotation of the note
        return [self.__object_story_mark]

# ---------------------------------------------------------------------------


class SpecialCharacter:
    """
    Abstract class for special characters subclasses.
    """

    def __init__(self, **kwargs):
        self._ctype = None

        for setting_name, setting_value in kwargs.items():
            if setting_name == "type":
                self.type = setting_value

    def _text_class(self, key: str) -> Any:
        return None

    def _check_class(self) -> Any:
        if self._ctype is None:
            return None

        textclass = self._text_class(self._ctype)

        if textclass is None:
            return None

        return textclass

    def _compile(self) -> list:
        textclass = self._check_class()

        if textclass is None:
            return []

        return [textclass()]

    def __str__(self) -> str:
        textclass = self._check_class()

        if textclass is None:
            msg = "Empty character name/type. Possible values: "
            msg += ", ".join(
                [f"'{name}'" for name in self._character_set().keys()]
            )
            msg += "."

            raise ValueError(msg)

        return textclass().text

    def _has_caracter(self, key) -> bool:
        return False

    def _character_set(self):
        return {}

    @property
    def type(self) -> str:
        """Character type/name."""
        return self._ctype

    @type.setter
    def type(self, value: str) -> NoReturn:
        if self._has_caracter(value):
            self._ctype = value
        else:
            msg = "Invalid character name/type. Possible values: "
            msg += ", ".join(
                [f"'{name}'" for name in self._character_set().keys()]
            )
            msg += "."
            raise ValueError(msg)


class Unicode(SpecialCharacter):
    """
    Unicode's special characters.
    """

    characters = {
        "joiner": model_stories.UnicodeJoiner,
        "non-joiner": model_stories.UnicodeNonJoiner,
    }

    def __repr__(self):
        return f"pyscribus.file.stories.Unicode(type='{self.type}')"

    def _text_class(self, key: str) -> Any:
        return Unicode.characters.get(key)

    def _character_set(self) -> dict:
        return Unicode.characters

    def _has_caracter(self, key) -> bool:
        return key in Unicode.characters


class Field(SpecialCharacter):
    """
    Variable in a paragraph.
    """

    characters = {
        "page": model_stories.PageNumberVariable,
        "pages": model_stories.PageCountVariable,
    }

    def __repr__(self):
        return f"pyscribus.file.stories.Field(type='{self.type}')"

    def _text_class(self, key: str) -> Any:
        return Variable.characters.get(key)

    def _character_set(self) -> dict:
        return Variable.characters

    def _has_caracter(self, key) -> bool:
        return key in Variable.characters


class Break(SpecialCharacter):
    """
    Line/Frame/Column break.
    """

    characters = {
        "line": model_stories.StoryLineBreak,
        "frame": model_stories.StoryFrameBreak,
        "column": model_stories.StoryColumnBreak,
    }

    def __repr__(self):
        return f"pyscribus.file.stories.Break(type='{self.type}')"

    def _text_class(self, key: str) -> Any:
        return Break.characters.get(key)

    def _character_set(self) -> dict:
        return Break.characters

    def _has_caracter(self, key) -> bool:
        return key in Break.characters


class Space(SpecialCharacter):
    """
    Space character in a paragraph.
    """

    characters = {
        "nbsp": model_stories.NonBreakingSpace,
        "narrownbsp": model_stories.NarrowNonBreakingSpace,
        "en": model_stories.EnSpace,
        "em": model_stories.EmSpace,
        "thin": model_stories.ThinSpace,
        "treeperem": model_stories.ThreePerEmSpace,
        "thick": model_stories.ThickSpace,
        "fourperem": model_stories.FourPerEmSpace,
        "mid": model_stories.MidSpace,
        "hair": model_stories.HairSpace,
    }

    def __repr__(self):
        return f"pyscribus.file.stories.Space(type='{self.type}')"

    def __str__(self) -> str:
        if self._ctype == "nbsp":
            # Contrary to other space classes, the regular non-breakable is
            # not a pyscribus.model.stories.TemporaryFragment
            return " "

        return SpecialCharacter.__str__(self)

    def _text_class(self, key: str) -> Any:
        return Space.characters.get(key)

    def _character_set(self) -> dict:
        return Space.characters

    def _has_caracter(self, key) -> bool:
        return key in Space.characters


# ---------------------------------------------------------------------------

AppendableToText = Union[str, Space, Unicode]
SpecialCharacters = Union[Unicode, Field, Break, Space]

# ---------------------------------------------------------------------------


class Fragment:
    """
    Abstract class for fragment of texts, with(out) formatting.
    """

    def __init__(self, **kwargs):
        self.sequence = []

        if (text := kwargs.get("text")) is not None:
            self.sequence = [text]

        if (sequence := kwargs.get("sequence")) is not None:
            self.sequence = sequence

    # --------------------------------------------------------------------

    def append(
        self, item: Union[AppendableToText, list[AppendableToText]]
    ) -> bool:
        """
        Append text, including special characters to this fragment of text.

        :type item: Union[AppendableToText, list[AppendableToText]]
        :param item: Text to append.
        :rtype: bool
        """

        if not isinstance(item, list):
            item = [item]

        for element in item:
            if not self.sequence:
                if isinstance(item, Space):
                    if item._ctype == "nbsp":
                        self.sequence = [item]
                    else:
                        self.sequence = [str(item)]

                    continue

                self.sequence = [str(item)]
                continue

            if isinstance(item, Space):
                if item._ctype == "nbsp":
                    self.sequence.append(item)
                    continue

            self.sequence.append(str(item))

        return True

    # Templating ---------------------------------------------------------

    def match_pattern(self, pattern) -> bool:
        """
        :param pattern: Compiled regex to match against this fragment of text
        :rtype: bool
        """

        for element in self.sequence:
            if isinstance(element, str):
                if pattern.findall(element) is not None:
                    return True

        return False

    def has_templating_key(self, templating_key: str) -> bool:
        for element in self.sequence:
            if isinstance(element, str):
                if templating_key in element:
                    return True

        return False

    def feed_templating(self, templating_key: str, value: str) -> NoReturn:
        """
        :type templating_key: str
        :param templating_key: Key to replace with value.
        :type value: str
        :param value: Value to replace key with.
        """
        for element in self.sequence:
            if isinstance(element, str):
                if templating_key in element:
                    element = element.replace(templating_key, value)

    # --------------------------------------------------------------------

    def _compile(self, context: Literal["story", "note"] = "story") -> list:
        if context == "story":
            return self._compile_story()

        return self._compile_note()

    def _compile_note(self) -> list:
        new_sequence = []
        return new_sequence

    def _compile_story(self) -> list:
        new_sequence = []

        for element in self.sequence:
            # Non breakable spaces are treated as distinct element

            if isinstance(element, Space):
                new_sequence += element._compile()
                continue

            if not new_sequence:
                if isinstance(element, str):
                    item = model_stories.StoryFragment()
                    item.text = element
                    new_sequence.append(item)
                    continue

            last_item = new_sequence[-1]

            if isinstance(element, str):
                if isinstance(last_item, model_stories.StoryFragment):
                    last_item.text += element
                    continue

                item = model_stories.StoryFragment()
                item.text = element
                new_sequence.append(item)
                continue

        return new_sequence

    def _load(self, model_object) -> bool:
        self.sequence = [model_object.text]
        return True

    # --------------------------------------------------------------------

    def __str__(self):
        return "".join([str(element) for element in self.sequence])

    def _repr_sequence(self):
        # This part merges strings in the sequence that are consecutive
        # together, and keeps other types as-is.

        tmp_sequence = []

        for element in self.sequence:
            if not tmp_sequence:
                if isinstance(element, str):
                    tmp_sequence.append(element)
                    continue

                tmp_sequence.append(element)
                continue

            if isinstance(element, str):
                if isinstance(tmp_sequence[-1], str):
                    tmp_sequence += element
                    continue

                tmp_sequence.append(element)
                continue

            tmp_sequence.append(element)
            continue

        # This part create a repr version of each element

        repr_sequence = []

        for element in tmp_sequence:
            if isinstance(element, str):
                repr_sequence.append(f"'{element}'")
            else:
                repr_sequence.append(element.__repr__())

        # And here we go

        return ", ".join(repr_sequence)


class SpanText(Fragment):
    """
    Formatted fragment of text in a paragraph
    """

    def __init__(self, **kwargs):
        Fragment.__init__(self, **kwargs)

        self._character_style = None

        self._font = {
            # TODO To implement from the model:
            # "size": False,
            # "opacity": False,
            "name": False,
            "color": False,
        }

        self._features = {
            "inherit": False,
            "strike": False,
            "smallcaps": False,
            "allcaps": False,
            "subscript": False,
            "superscript": False,
            "underline": False,
            "underlinewords": False,
        }

        for setting_name, setting_value in kwargs.items():
            if setting_name == "character_style":
                self.character_style = setting_value
            if setting_name == "font":
                self.font = setting_value
            if setting_name == "color":
                self.color = setting_value
            if setting_name == "strike":
                self.strike = setting_value
            if setting_name == "smallcaps":
                self.smallcaps = setting_value
            if setting_name == "allcaps":
                self.allcaps = setting_value
            if setting_name == "subscript":
                self.subscript = setting_value
            if setting_name == "superscript":
                self.superscript = setting_value
            if setting_name == "underline":
                self.underline = setting_value
            if setting_name == "underlinewords":
                self.underlinewords = setting_value
            if setting_name == "inherit":
                self.inherit = setting_value

    def _compile_story(self) -> list:
        new_sequence = Fragment._compile_story(self)

        new_sequence[0].parent_character = self._character_style
        new_sequence[0].features = self._features

        for pyf_key in self._font.keys():
            new_sequence[0].font[pyf_key] = self._font[pyf_key]

        return new_sequence

    def _compile_note(self) -> list:
        return Fragment._compile_note(self)

    def _load(self, model_object) -> bool:
        Fragment._load(self, model_object)

        if model_object.parent_character is not None:
            self._character_style = model_object.parent_character

        for pyf_key in self._font.keys():
            self._font[pyf_key] = model_object.font[pyf_key]

        for pyf_key in self._features.keys():
            self._features[pyf_key] = model_object.features[pyf_key]

        return True

    # --------------------------------------------------------------------

    def __repr__(self):
        repr = "pyscribus.file.stories.SpanText("
        repr += f"sequence=[{self._repr_sequence()}]"

        # -----------------------------------------------------------

        if self._character_style is not None:
            repr += f", character_style='{self._character_style}'"

        # -----------------------------------------------------------

        args = []

        for font_key, font_value in self._font.items():
            if font_value:
                args.append([font_key, font_value])

        for feature_key, feature_value in self._features.items():
            if feature_value:
                args.append([feature_key, feature_value])

        if not args:
            return repr + ")"

        repr += ", "

        repr_args = []

        for init_key, arg_key in [
            ["font", "name"],
            ["color", "color"],
            ["strike", "strike"],
            ["inherit", "inherit"],
            ["allcaps", "allcaps"],
            ["smallcaps", "smallcaps"],
            ["subscript", "subscript"],
            ["superscript", "superscript"],
            ["underline", "underline"],
            ["underlinewords", "underlinewords"],
        ]:
            for arg in args:
                if arg[0] != arg_key:
                    continue

                if init_key in ["font", "color"]:
                    repr_args.append(f"{init_key}='{arg[1]}'")
                else:
                    repr_args.append(f"{init_key}={arg[1]}")

                break

        # -----------------------------------------------------------

        repr += ", ".join(repr_args)

        return repr + ")"

    # --------------------------------------------------------------------

    @property
    def inherit(self) -> bool:
        return self._features["inherit"]

    @inherit.setter
    def inherit(self, value) -> NoReturn:
        self._features["inherit"] = bool(value)

    # --------------------------------------------------------------------

    @property
    def superscript(self) -> bool:
        """Compose in superscript?"""
        return self._features["superscript"]

    @superscript.setter
    def superscript(self, value) -> NoReturn:
        value = bool(value)

        self._features["superscript"] = value

        if value:
            self._features["subscript"] = False

    # --------------------------------------------------------------------

    @property
    def underlinewords(self) -> bool:
        """Underline text words?"""
        return self._features["underline"]

    @underlinewords.setter
    def underlinewords(self, value) -> NoReturn:
        value = bool(value)

        self._features["underlinewords"] = value

        if value:
            self._features["underline"] = False

    # --------------------------------------------------------------------

    @property
    def underline(self) -> bool:
        """Underline text?"""
        return self._features["underline"]

    @underline.setter
    def underline(self, value) -> NoReturn:
        value = bool(value)

        self._features["underline"] = value

        if value:
            self._features["underlinewords"] = False

    # --------------------------------------------------------------------

    @property
    def subscript(self) -> bool:
        """Compose in subscript?"""
        return self._features["subscript"]

    @subscript.setter
    def subscript(self, value) -> NoReturn:
        value = bool(value)

        self._features["subscript"] = value

        if value:
            self._features["superscript"] = False

    # --------------------------------------------------------------------

    @property
    def strike(self) -> bool:
        """Strikethrough text?"""
        return self._features["strike"]

    @strike.setter
    def strike(self, value) -> NoReturn:
        self._features["strike"] = bool(value)

    # --------------------------------------------------------------------

    @property
    def allcaps(self) -> bool:
        """Compose in all caps?"""
        return self._features["allcaps"]

    @allcaps.setter
    def allcaps(self, value) -> NoReturn:
        value = bool(value)

        self._features["allcaps"] = value

        if value:
            self._features["smallcaps"] = False

    # --------------------------------------------------------------------

    @property
    def smallcaps(self) -> bool:
        """Compose in small caps?"""
        return self._features["smallcaps"]

    @smallcaps.setter
    def smallcaps(self, value) -> NoReturn:
        value = bool(value)

        self._features["smallcaps"] = value

        if value:
            self._features["allcaps"] = False

    # --------------------------------------------------------------------

    @property
    def font(self) -> Union[None, str]:
        """
        Font name, or ``None``.

        :rtype: Union[None, str]
        """
        if not self._font["name"]:
            return None

        return self._font["name"]

    @font.setter
    def font(self, value: str) -> NoReturn:
        self._font["name"] = str(value)

    # --------------------------------------------------------------------

    @property
    def color(self) -> Union[None, str]:
        """
        Font color, or ``None``.

        :rtype: Union[None, str]
        """
        if not self._font["color"]:
            return None

        return self._font["color"]

    @color.setter
    def color(self, value: str) -> NoReturn:
        self._font["color"] = str(value)

    # --------------------------------------------------------------------

    @property
    def character_style(self) -> Union[str, None]:
        """
        Default character style applied in this fragment of text.

        ``None`` if the style is Scribus' default character style.
        """
        return self._character_style

    @character_style.setter
    def character_style(self, value: Union[str, None]) -> NoReturn:
        # TODO: Verify if the style exists
        self._character_style = str(value)

    # --------------------------------------------------------------------


class Text(Fragment):
    """
    Fragment of text in a paragraph, without any formatting.
    """

    def __repr__(self):
        return (
            f"pyscribus.file.stories.Text(sequence=[{self._repr_sequence()}])"
        )

    def _compile_note(self) -> list:
        return Fragment._compile_note(self)


# ---------------------------------------------------------------------------

AppendableToParagraph = Union[Text, SpanText, Field, Space, Break, Unicode, Note]

# ---------------------------------------------------------------------------


class Paragraph:
    """
    Paragraph in a story.

    High-level wrapper around many elements that can make a paragraph in a
    Scribus story.

    (This class doesn't match any SLA element itself)
    """

    def __init__(self, parent_story: Story, **kwargs):
        # Parent story
        self.story = parent_story
        # Stories interface
        self.parent = parent_story.parent
        # ScribusFile
        self.file = parent_story.file

        self.sequence = []

        self._paragraph_style = None

        if (seq := kwargs.get("sequence")) is not None:
            self.sequence = seq

        for setting_name, setting_value in kwargs.items():
            if setting_name == "style":
                self.style = setting_value

    # --------------------------------------------------------------------

    def __str__(self):
        text = ""

        for element in self.sequence:
            text += str(element)

        return text

    # --------------------------------------------------------------------

    def _load(self, model_sequence: list, **kwargs) -> bool:
        for element in model_sequence:

            if isinstance(element, model_stories.StoryFragment):
                if element._has_formatting():
                    new_element = SpanText()
                else:
                    new_element = Text()

                was_loaded = new_element._load(element)

                if was_loaded:
                    self.sequence.append(new_element)

            elif isinstance(element, model_stories.StoryParagraphEnding):
                # Nothing is added to sequence
                self.style = element.parent

            if isinstance(element, model_marks.StoryNoteMark):
                new_note = Note(self.file)
                was_loaded = new_note._load(element)

                if was_loaded:
                    self.sequence.append(new_note)

        return True

    def _compile(self, context: Literal["story", "note"] = "story") -> list:
        if context == "story":
            return self._compile_story()

        return self._compile_note()

    def _compile_note(self) -> list:
        new_sequence = []
        return new_sequence

    def _compile_story(self) -> list:
        new_sequence = []

        for element in self.sequence:
            # Special characters classes do not require context argument
            if isinstance(element, (Unicode, Field, Break, Space)):
                new_sequence += element._compile()
            else:
                new_sequence += element._compile("story")

        # End the paragraph -----------------------------------------

        paragraph_ending = model_stories.StoryParagraphEnding()

        if self._paragraph_style is not None:
            paragraph_ending.parent = self._paragraph_style

        new_sequence.append(paragraph_ending)

        # -----------------------------------------------------------

        return new_sequence

    # --------------------------------------------------------------------

    def __iter__(self):
        """
        Special method to make Paragraph class iterable.
        """
        for value in self.sequence:
            yield value

    # --------------------------------------------------------------------

    def new_note(self):
        new_note = Note(self.parent, new=True)
        return new_note

    @property
    def notes(self) -> list[Note]:
        """
        Notes called inside the paragraph.

        :rtype: list[Note]
        """
        return [
            seq_element
            for seq_element in self.sequence
            if isinstance(seq_element, Note)
        ]

    # --------------------------------------------------------------------

    @property
    def style(self) -> Union[str, None]:
        """
        Paragraph style applied in this paragraph.

        ``None`` if the style is Scribus' default paragraph style.
        """
        return self._paragraph_style

    @style.setter
    def style(self, value: Union[str, None]) -> NoReturn:
        # TODO: Verify if the style exists
        self._paragraph_style = str(value)

    # --------------------------------------------------------------------

    def append(
        self, item: Union[AppendableToParagraph, list[AppendableToParagraph]]
    ) -> bool:
        """
        Append content to this paragraph.

        :param item: Item, or list of items to append
        :type item: Union[AppendableToParagraph, list[AppendableToParagraph]]
        :rtype: bool
        :returns: Success.
        """

        def valid_item(sub_item) -> bool:
            for item_class in [Text, SpanText, Field, Space, Break, Unicode, Note]:
                if isinstance(sub_item, item_class):
                    return True

            return False

        if isinstance(item, list):
            for element in item:
                if valid_item(element):
                    self.sequence.append(element)

            return True

        if not valid_item(item):
            return False

        self.sequence.append(item)

        return True

    # --------------------------------------------------------------------

    def __notes(self) -> list:
        """
        .. warning:: Not implemented.

        """
        raise NotImplementedError()


# ---------------------------------------------------------------------------

AppendableToStory = Union[Paragraph]

# ---------------------------------------------------------------------------


class Story:
    """
    High-level wrapper around model's ``Story`` class.
    """

    def __init__(self, parent_interface: Stories, **kwargs):
        self.parent = parent_interface
        self.file = parent_interface.file
        self.frame = None

        self.sequence = []
        self.object = None

        self._paragraph_style = None
        self._character_style = None

        if (is_new := kwargs.get("new")) is not None:
            if is_new:
                self._new(**kwargs)

        if (seq := kwargs.get("sequence")) is not None:
            self.sequence = seq
            self.update()

        if (mstory := kwargs.get("model_object")) is not None:
            self._load(mstory, **kwargs)

    # --------------------------------------------------------------------

    def _new(self, **kwargs):
        self.object = model_stories.Story()

    def _load(self, model, **kwargs):
        self.object = model
        # FIXME TODO Load self.sequence with interfaces to model object
        # content.

        content_by_pars = self.object.bypars()

        if not content_by_pars:
            return

        # Get the first paragraph, because it contains the DefaultStyle

        first_para = content_by_pars.pop(0)
        default_style = first_para.pop(0)
        self._paragraph_style = default_style.parent_paragraph
        self._character_style = default_style.parent_character

        # Creating the paragraphs

        content_by_pars = [first_para] + content_by_pars

        for model_paragraph in content_by_pars:
            new_p = self.new_paragraph()
            was_loaded = new_p._load(model_paragraph)

            if was_loaded:
                self.sequence.append(new_p)

        return True

    # --------------------------------------------------------------------

    def new_paragraph(self, **kwargs) -> Paragraph:
        item = Paragraph(self, **kwargs)
        return item

    # --------------------------------------------------------------------

    def update(self):
        new_sequence = self._compile()
        self.object.sequence = new_sequence

    def _compile(self) -> list:
        new_sequence = []

        # Style initialisation of the story -------------------------

        default_style = model_stories.StoryDefaultStyle()
        default_style.parent_paragraph = self._paragraph_style
        default_style.parent_character = self._character_style

        new_sequence.append(default_style)

        # Append sub element sequences to own sequence --------------

        for element in self.sequence:
            new_sequence += element._compile()

        # -----------------------------------------------------------

        model_doc = self.file.model.document
        doc_marks = []
        doc_notes = []

        print("Frame", self.frame)

        for element in self.sequence:
            if not isinstance(element, Paragraph):
                continue

            if not element.notes:
                continue

            for note in element.notes:
                # Adding the note mark in the global list of marks ----------
                # (The note mark in the story has already been added through
                # the regular Paragraph._compile method.)

                doc_mark = note._compile("mark")

                if doc_mark.label in doc_marks:
                    continue

                model_doc.marks.append(doc_mark)
                doc_marks.append(doc_mark.label)

                # Note frame item and note frame itself ---------------------

                # Create the note frame item

                note_frame_item = note._compile("frame")
                note_frame_item.story_frame_id = self.frame

                model_note_frames_ids = [
                    model_note_frame.story_frame_id
                    for model_note_frame in model_doc.notes_frames
                ]

                add_note_frame_item = False

                if note_frame_item.story_frame_id not in model_note_frames_ids:
                    add_note_frame_item = True

                # Create the note frame ?

                if note_frame_item.own_frame_id is None:
                    parent_frame = self.file.pageobjects.filter(
                        object_id=self.frame
                    )[0]

                    display_frame = self.file.pageobjects.new_text(
                        pos_x = parent_frame.pos_x,
                        pos_y = parent_frame.pos_y + parent_frame.height,
                        default = "notes"
                    )
                    display_frame.object.welding["items"][0].target = self.frame

                    style = note._compile("note").style

                    display_frame.object.object_id = f"notes_{style}"
                    display_frame.object.welding["source"] = f"notes_{style}"
                    note_frame_item.own_frame_id = f"notes_{style}"

                    self.file.pageobjects.append(display_frame)
                else:
                    display_frame = self.file.pageobjects.filter(
                        object_id=note_frame_item.own_frame_id
                    )

                # Add the note frame item ?

                if add_note_frame_item:
                    # The own_frame must be set before
                    model_doc.notes_frames.append(note_frame_item)

                # -----------------------------------------------------------

                # NOTE FIXME This part works but a text frame and note frame
                # are also necessary
                # note_content = note._compile("note")

                # if note_content.parent_mark in doc_notes:
                    # continue

                # model_doc.notes.append(note_content)
                # doc_notes.append(note_content.parent_mark)

            # TODO Ask parent document to insert notes content, frame, etc.
            # Note mark in the story is already added through previous lines

        print(model_doc.marks)
        print(model_doc.notes)

        # -----------------------------------------------------------

        last_paragraph_parent = None

        # If there no information in the last paragraph ending, then remove it.
        if isinstance(new_sequence[-1], model_stories.StoryParagraphEnding):
            last_paragraph_parent = new_sequence[-1].parent
            new_sequence.pop(-1)

        if not last_paragraph_parent:
            last_paragraph_parent = None

        story_ending = model_stories.StoryEnding()
        story_ending.parent = last_paragraph_parent

        new_sequence.append(story_ending)

        return new_sequence

    # --------------------------------------------------------------------

    @property
    def style(self) -> Union[str, None]:
        """
        Default paragraph style applied in this story.

        ``None`` if the style is Scribus' default paragraph style.

        Can be overrided by individual paragraph inside the story
        """
        return self._paragraph_style

    @style.setter
    def style(self, value: Union[str, None]) -> NoReturn:
        # TODO: Verify if the style exists
        self._paragraph_style = str(value)

    # --------------------------------------------------------------------

    @property
    def character_style(self) -> Union[str, None]:
        """
        Default character style applied in this story.

        ``None`` if the style is Scribus' default character style.

        Can be overrided by individual paragraph inside the story.
        """
        return self._character_style

    @character_style.setter
    def character_style(self, value: Union[str, None]) -> NoReturn:
        # TODO: Verify if the style exists
        self._character_style = str(value)

    # --------------------------------------------------------------------

    @property
    def notes(self) -> list[Note]:
        """
        Notes called inside the story.

        :rtype: list[Note]
        """
        notes = []

        for element in self.sequence:
            if not isinstance(element, Paragraph):
                continue

            notes += element.notes

        return notes

    # --------------------------------------------------------------------

    def append(
        self, item: Union[AppendableToStory, list[AppendableToStory]]
    ) -> bool:
        """
        Append something to this story.

        :param item: Item, or list of items to append
        :type item: Union[AppendableToStory, list[AppendableToStory]]
        :rtype: bool
        :returns: Success.
        """

        def valid_item(sub_item):
            return isinstance(sub_item, Paragraph)

        if isinstance(item, list):
            for element in item:
                if valid_item(element):
                    self.sequence.append(element)

            return True

        if not valid_item(item):
            return False

        self.sequence.append(item)

        self.update()

        return True

    # --------------------------------------------------------------------

    def __notes(self) -> list:
        """
        Returns the list of notes used in this story.

        .. warning:: Not implemented.

        """
        raise NotImplementedError()


# ---------------------------------------------------------------------------


class Stories:
    """
    Wrapper around Scribus stories.

    :type file: pyscribus.file.ScribusFile
    :param file: Scribus file wrapper
    :type templatable: bool
    :param templatable: Only load templatable stories

    :Example:

    .. code:: python

       from pyscribus.file import ScribusFile
       from pyscribus.file import Paragraph

       doc = ScribusFile("1.5.5", "example.sla")

       len(doc.stories)
       first_story = doc.stories[0]
       text = doc.stories.rawtext(first_story)

       for story in doc.stories.templatable:
           doc.stories.feed(story, data)

       # BELOW THIS LINE, EVERYTHING IS NOT IMPLEMENTED --------

    """

    def __init__(self, file: pyf.ScribusFile, templatable: bool = False):
        self.file = file
        self.model = file.model

        self.only_templatable = templatable

        # Handling a private register of all (end|foot)notes counts by style,
        # because not having that register would make PyScribus create notes
        # using 1 as count everytime.
        self._notes_counts = {}
        self._update_notes_count()

    def _has_notes_styled(self, note_style: str) -> bool:
        if note_style not in self._notes_counts:
            return False

        if not self._notes_counts[note_style]:
            return False

        return True

    def _increase_notes_count(self, note_style: str) -> int:
        """
        Return a new note (using ``note_style`` style) number.

        :rtype: int
        """
        if note_style not in self._notes_counts:
            self._notes_counts[note_style] = 1
            return 1

        self._notes_counts[note_style] += 1

        return copy.deepcopy(self._notes_counts[note_style])

    def _update_notes_count(self) -> bool:
        """
        This update an internal notes counter, referencing counts by note
        styles.

        :rtype: bool
        :returns: ``True`` if there is no errors.
        """

        labels = [
            mark.label
            for mark in self.model.document.marks
            if mark.type == "note"
        ]

        if not labels:
            self._notes_counts["Default"] = 0
            return True

        for label in labels:
            _, style, count = label.split("_")

            count = int(count)

            if style not in self._notes_counts:
                self._notes_counts[style] = 0

            if count > self._notes_counts[style]:
                self._notes_counts[style] = count

        return True

    def __len__(self) -> int:
        """

        :Example:

        .. code:: python

           from pyscribus.file import ScribusFile
           doc = ScribusFile("1.5.5", "example.sla")
           len(doc.stories)

        """
        return len(self.__stories())

    def __getitem__(self, item: int):
        """

        :Example:

        .. code:: python

           from pyscribus.file import ScribusFile
           doc = ScribusFile("1.5.5", "example.sla")
           first_story = doc.stories[0]

        """
        return self.__stories()[item]

    def new_story(self, **kwargs) -> Story:
        return Story(self, new=True, **kwargs)

    @property
    def items(self) -> list:
        """List of all stories."""
        return self.__stories()

    @property
    def templatable(self) -> list[Story]:
        """
        Stories that contains templatable elements.

        :Example:

        .. code:: python

           from pyscribus.file import ScribusFile
           doc = ScribusFile("1.5.5", "example.sla")

           for story in doc.stories.templatable:
               doc.stories.feed(story, data)

        """
        return [
            story
            for story in self.__stories()
            if self.__story_is_templatable(story)
        ]

    def rawtext(self, story: Union[Story, model_stories.Story]) -> str:
        """
        Returns a text string equivalent to *what Scribus story editor
        saves* as txt file:

        - Non breaking spaces are converted into spaces
        - Line breaks are converted into spaces
        - Text formatting, frame and column breaks are not saved

        :Example:

        .. code:: python

           from pyscribus.file import ScribusFile
           doc = ScribusFile("1.5.5", "example.sla")

           story = doc.stories[1]
           text = doc.stories.rawtext(story)

        :rtype: string
        """

        if isinstance(story, Story):
            story._compile()
            paragraphs = story.object.bypars()
        elif isinstance(story, model_stories.Story):
            paragraphs = story.bypars()
        else:
            raise TypeError("story argument is not a Scribus story instance.")

        text = ""

        for paragraph in paragraphs:
            for element in paragraph:
                # Text formatting is not saved
                if isinstance(element, model_stories.StoryFragment):
                    text += element.text
                if isinstance(element, model_stories.NonBreakingSpace):
                    text += " "
                # Line breaks are exported by Scribus... as spaces
                if isinstance(element, model_stories.StoryLineBreak):
                    text += " "
                # We dont care about StoryFrameBreak and StoryColumnBreak
                if isinstance(element, model_stories.StoryParagraphEnding):
                    text += "\n"

        return text

    def feed(
        self, story: Story, datas: dict
    ) -> Union[bool, list[Union[Text, SpanText]]]:
        """
        Feed template-able datas into this story.

        :Example:

        .. code:: python

           from pyscribus.file import ScribusFile
           doc = ScribusFile("1.5.5", "example.sla")

           story = doc.stories[0]
           doc.stories.feed(story, data)

        :type story: pyscribus.model.stories.Story
        :type datas: dict
        :returns: List of modified elements in this story, or False
        :rtype: Union[bool, list[pyscribus.file.stories.Text, pyscribus.file.stories.SpanText]]
        """

        templatable_elements = self.__story_templatable(story)

        if not templatable_elements:
            return False

        modified = []

        for element in templatable_elements:
            # Iterating over each data to fill

            for template_key, template_value in datas.items():
                # Nothing to feed into the templatable elements

                # if template_key not in element.text:
                # continue
                if not element.has_templating_key(template_key):
                    continue

                new_value = None

                if isinstance(datas[template_key], list):
                    # We have %key% -> ["Value1", "Value2", …]
                    # So the first occurrence of %key% is "Value1", the
                    # second "Value2", etc.

                    if len(datas[template_key]):
                        new_value = datas[template_key].pop(0)
                else:
                    # We have %key% -> "Value"
                    new_value = template_value

                if new_value is None:
                    continue

                # element.text = element.text.replace(template_key, new_value)
                element.feed_templating(template_key, new_value)

                modified.append(element)

        return modified

    def __story_templatable(self, story: Story) -> list:
        """
        Return elements of Story sequence that are available for templating.

        :type story: pyscribus.file.stories.Story
        :rtype: list
        :returns: List of pyscribus.stories.StoryFragment
        """

        contents = []

        pattern = self.file.templating["intext-pattern"]

        # for element in story.object.sequence:
        #     if isinstance(element, model_stories.StoryFragment):
        #         if pattern.findall(element.text) is not None:
        #             contents.append(element)

        for paragraph in story.sequence:
            for par_element in paragraph:
                if isinstance(par_element, (Text, SpanText)):
                    if par_element.match_pattern(pattern):
                        contents.append(par_element)

        return contents

    def __story_is_templatable(self, story: Story) -> bool:
        templatable = self.__story_templatable(story)

        # pattern = self.file.templating["intext-pattern"]

        # for element in story.model_object.sequence:
        #     if isinstance(element, model_stories.StoryFragment):
        #         if pattern.findall(element.text) is not None:
        #             return True

        # return False

        return bool(templatable)

    @pyfe.has_document
    def __stories(self) -> list[Story]:
        """
        Returns all stories in the document.

        :rtype: list[pyscribus.file.stories.Story]
        :returns: List of stories
        """

        stories = []

        # Text frames stories --------------------------------------------

        filtered = [
            page_object
            for page_object in self.model.document.page_objects
            if page_object.have_stories and page_object.stories
        ]

        if filtered:
            for page_object in filtered:
                stories.extend(page_object.stories)

        # Table cells stories --------------------------------------------

        tables = [
            page_object
            for page_object in self.model.document.page_objects
            if page_object.ptype == "table"
        ]

        if tables:
            cells = []

            for page_object in tables:
                cells.extend(page_object.cells)

            for cell in cells:
                if cell.story is not None:
                    stories.append(cell.story)

        # Convert stories ------------------------------------------------

        # This converts stories from the model subpackage to stories
        # from the file subpackage.
        stories = [Story(self, model_object=story) for story in stories]

        # ----------------------------------------------------------------

        if self.only_templatable:
            return [
                story
                for story in stories
                if self.__story_is_templatable(story)
            ]

        return stories


# vim:set shiftwidth=4 softtabstop=4:
