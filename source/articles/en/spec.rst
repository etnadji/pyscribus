************************
Document-able attributes
************************

DOCUMENT
========

Omitted XPath : ``/SCRIBUSUTF8NEW/DOCUMENT``.

+---------------------+------------------------+-------------+-------------+----------+
| Path                | Explanation            | Value       | Default     | Optional |
+=====================+========================+=============+=============+==========+
| ``/@DPInCMYK``      | ICC profile for CMYK   | string      |             | Yes      |
|                     | images                 |             |             |          |
+---------------------+------------------------+-------------+-------------+----------+
| ``/@DPIn3``         | ICC profile for CMYK   | string      |             | Yes      |
|                     | colors                 |             |             |          |
+---------------------+------------------------+-------------+-------------+----------+
| ``/@PAGEC``         | Scribus GUI page       | hexadecimal | ``#ffffff`` |          |
|                     | background             | color       |             |          |
+---------------------+------------------------+-------------+-------------+----------+
| ``/@SHOWFRAME``     | Show the page objects  | integer as  | ``1``       | No       |
|                     | frames                 | boolean     |             |          |
+---------------------+------------------------+-------------+-------------+----------+
| ``/@showBleed``     | Show documents bleeds  | integer as  | ``1``       | No       |
|                     |                        | boolean     |             |          |
+---------------------+------------------------+-------------+-------------+----------+
| ``/@DCOL``          | Default number of      | integer     | ``1``       | No       |
|                     | columns in text frames |             |             |          |
+---------------------+------------------------+-------------+-------------+----------+
| ``/@GapHorizontal`` | Gap in the scratch     | length in   | ``0``       | No       |
|                     | space between pages    | picas       |             |          |
|                     | (horizontal)           |             |             |          |
+---------------------+------------------------+-------------+-------------+----------+
| ``/@GapVertical``   | Gap in the scratch     | length in   | ``40``      | No       |
|                     | space between pages    | picas       |             |          |
|                     | (vertical)             |             |             |          |
+---------------------+------------------------+-------------+-------------+----------+

Attributes ``@MINGRID``, ``@MAJGRID``, ``@BASEGRID``, ``@BASEO`` are **not** 
optional.

DOCUMENT/LAYERS
---------------

Omitted XPath : ``/SCRIBUSUTF8NEW/DOCUMENT/LAYERS``.

+--------------+-----------------------+-------------+---------+----------+
| Path         | Explanation           | Value       | Default | Optional |
+==============+=======================+=============+=========+==========+
| ``/@FLOW``   |                       | integer     |         |          |
|              |                       | as boolean  |         |          |
+--------------+-----------------------+-------------+---------+----------+
| ``/@TRANS``  | Layer’s opacity       | percentage  | ``1``   |          |
|              |                       | 0 -> 1      |         |          |
+--------------+-----------------------+-------------+---------+----------+
| ``/@BLEND``  | Layer blend mode      |             |         |          |
+--------------+-----------------------+-------------+---------+----------+
| ``/@OUTL``   | Flag for wireframe    | integer     | ``0``   |          |
|              | view                  | as boolean  |         |          |
+--------------+-----------------------+-------------+---------+----------+
| ``/@LAYERS`` | Color outline of the  | hexadecimal |         |          |
|              | layer’s frames        | color       |         |          |
+--------------+-----------------------+-------------+---------+----------+

DOCUMENT/CHARSTYLE
------------------

Omitted XPath : ``/SCRIBUSUTF8NEW/DOCUMENT/CHARSTYLE``.

+---------------------+--------------------+------------+---------+----------+
| Path                | Explanation        | Value      | Default | Optional |
+=====================+====================+============+=========+==========+
| ``/@DefaultStyle``  | Same as STYLE      | String     |         |          |
+---------------------+--------------------+------------+---------+----------+
| ``/@wordTrack``     | Space width        | Percentage |         |          |
|                     |                    |            |         |          |
|                     |                    | 0 -> 1     |         |          |
+---------------------+--------------------+------------+---------+----------+
| ``/@KERN``          | Kerning            | Percentage |         |          |
|                     |                    |            |         |          |
|                     |                    | 0 -> 100   |         |          |
+---------------------+--------------------+------------+---------+----------+
| ``/@HyphenWordMin`` | Minimal number of  | Integer    |         |          |
|                     |                    |            |         |          |
|                     | caracters for word | >= 3       |         |          |
|                     |                    |            |         |          |
|                     | for word           |            |         |          |
|                     | hyphenation        |            |         |          |
+---------------------+--------------------+------------+---------+----------+

DOCUMENT/Sections
-----------------

Omitted XPath : ``/SCRIBUSUTF8NEW/DOCUMENT/Sections/Section``.

+------------------+-------------+--------------------+------------------+----------+
| Path             | Explanation | Value              | Default          | Optional |
+==================+=============+====================+==================+==========+
| ``/@FillChar``   |             | Unicode code       | ``0``            |          |
|                  |             |                    |                  |          |
|                  |             | point of character | (NULL character) |          |
+------------------+-------------+--------------------+------------------+----------+
| ``/@FieldWidth`` |             |                    | ``0``            |          |
+------------------+-------------+--------------------+------------------+----------+

DOCUMENT/PDF
------------

Omitted XPath : ``/SCRIBUSUTF8NEW/DOCUMENT/PDF``.

+--------------+--------------------------+--------+-------------------------------------+----------+
| Path         | Explanation              | Value  | Default                             | Optional |
+==============+==========================+========+=====================================+==========+
| ``/@ImageP`` | Color profile for images | string | ``sRGB display profile (ICC v2.2)`` | No       |
+--------------+--------------------------+--------+-------------------------------------+----------+

``@UseProfiles``, ``@UseProfiles2``, ``@SolidP``, ``@ImageP``, ``@PrintP`` are not optional. 

DOCUMENT/PAGEOBJECT
-------------------

Omitted XPath : ``/SCRIBUSUTF8NEW/DOCUMENT/PAGEOBJECT``.

+--------------------------------+--------------------+----------------------+-------------------+----------+
| Path                           | Explanation        | Value                | Default           | Optional |
+================================+====================+======================+===================+==========+
| ``/@path``                     | Path data          | SVG path/@d string   | ``0``             |          |
+--------------------------------+--------------------+----------------------+-------------------+----------+
| ``/@copath``                   | Path data          | Same as @path        |                   |          |
+--------------------------------+--------------------+----------------------+-------------------+----------+
| ``[@PTYPE="4"]/@VAlign``       | Vertical alignment | Same as @path        | ``0``             | No       |
|                                |                    |                      |                   |          |
|                                | of the text within | 0 = top              |                   |          |
|                                |                    |                      |                   |          |
|                                | the frame          | 1 = center           |                   |          |
|                                |                    |                      |                   |          |
|                                |                    | 2 = bottom           |                   |          |
+--------------------------------+--------------------+----------------------+-------------------+----------+
| ``[@PTYPE="4"]/@TEXTFLOWMODE`` | Flowing text mode  | Same as @path        | No attribute when | Yes      |
|                                |                    |                      |                   |          |
| ``[@PTYPE="6"]/@TEXTFLOWMODE`` |                    | 1 = Around shape     | no text flow.     |          |
|                                |                    |                      |                   |          |
| ``[@PTYPE="7"]/@TEXTFLOWMODE`` |                    | 2 = Around rectangle |                   |          |
|                                |                    |                      |                   |          |
|                                |                    | 3 = Around outline   |                   |          |
+--------------------------------+--------------------+----------------------+-------------------+----------+

Notes
=====

``/PAGEOBJECT/@path`` and ``/PAGEOBJECT/@copath`` format specifications are 
`available on W3C <https://www.w3.org/TR/SVG/paths.html#TheDProperty>`_.

Attributes ``@RATIO``, ``@SCALETYPE``, ``@LOCALSCX``, ``@LOCALSCY``, 
``@LOCALX``, ``@LOCALY``, ``@LOCALROT``, ``@PICART`` 
of ``/PAGEOBJECT/`` are **not** optional.

Corrections
===========

For ``Sections/Section/@Type``, the possible values are incomplete in the wiki.

The complete possible values are :

- Type_1_2_3
- Type_1_2_3_ar
- Type_i_ii_iii
- Type_I_II_III
- Type_a_b_c
- Type_A_B_C
- Type_Alphabet_ar
- Type_Abjad_ar
- Type_Hebrew
- Type_asterix
- Type_CJK

``DOCUMENT/@UNITS`` seems optional only if the document use points (≠ picas).

Notable things
==============

``/Marks/Mark[@type="3"]`` (variable text mark) ``@str`` attribute is used like 
the ``@label`` attribute of ``/Marks/Mark[@type!="3"]``.
