#!/usr/bin/python3
# -*- coding:Utf-8 -*-

"""
Templating system test for PyScribus.

Produce test-templated.sla using tests/templating.sla
as template document.
"""

import pyscribus.file as psf

if __name__ == "__main__":
    # templating=True activate templating functions

    template = psf.ScribusFile("1.5.5", "tests/templating_all.sla", templating=True)

    # The data that will be injected into the document

    datas = {
        "%title%": "A very formal document",
        "%date%": "1936",
        "%sentenceLeft%": [
            "Le droit à la vie comporte :",
            "1o Le droit à un travail assez réduit pour laisser des loisirs, assez rémunéré pour que tous aient largement part au bien-être que les progrès de la science et de la technique rendent de plus en plus accessibles, et qu’une répartition équitable doit et peut assurer à tous.",
            "2o Le droit à la pleine culture intellectuelle, morale, artistique et technique des facultés de chacun ;"
            "3o Le droit à la subsistance pour tous ceux qui sont incapables de travailler.",
            "Tous les travailleurs ont le droit de concourir personnellement ou par leurs représentants, à l’établissement des plans de production et de répartition, et d’en surveiller l’application de telle sorte qu’il n’y ait jamais exploitation de l’homme par l’homme, mais toujours juste rémunération du travail et utilisation pour le bien de tous, des puissances de création exaltées par la science.",
            "La propriété individuelle n’est un droit que lorsqu’elle ne porte aucun préjudice à l’intérêt commun.",
            "L’indépendance des citoyens et de l’État étant particulièrement menacée par la propriété qui prend la forme de groupements d’intérêts égoïstes et dominateurs (cartels, trusts, consortiums bancaires) les fonctions que cette propriété a usurpées doivent faire retour à la nation."
        ],
        "%Following%": "p. 4",
    }

    # Get all stories of the template that contains
    # template-able elements.

    # Templatable in-text elements are found with a regular
    # expression defined in sla.SLA.templating, like other
    # templating options.

    # We use the default regular expression here so
    # placeholders are surrounded by %

    # The search for placeholders is also case insensitive
    # by default.

    stories = template.stories.templatable()

    # Iterate over each story that contains template-able elements,
    # and injects the datas.

    for story in stories:
        template.stories.feed(story, datas)

    # Save the new document

    template.save("tests-outputs/test-templated.sla")

# vim:set shiftwidth=4 softtabstop=4:
