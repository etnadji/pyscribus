#!/usr/bin/python3
# -*- coding:Utf-8 -*-

"""
"""

import pyscribus.file as psf

if __name__ == "__main__":
    scribus_file = psf.ScribusFile("", "tests/metadatas.sla")

    # Just to avoid writing scribus_file.metadatas each time
    doc_metas = scribus_file.metadatas

    print(doc_metas.author)
    print(doc_metas.title)
    print()

    print("Keywords:", doc_metas.keywords)
    # This one will be added, as "Keyword_2" already exists
    doc_metas.add_keyword("Keyword_2")
    doc_metas.add_keyword("Keyword_3")
    print("=>", doc_metas.keywords)

    print()
    print("Metadatas as a list:")
    print(list(doc_metas))

    print()
    print("Metadatas as a dictionary:")
    print(dict(doc_metas))

    scribus_file.save("tests-outputs/metadatas.sla")

# vim:set shiftwidth=4 softtabstop=4:
