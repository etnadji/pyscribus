************
Scribus file
************

.. automodule:: pyscribus.file
    :members:
    :undoc-members:
    :show-inheritance:

pyscribus.file.colors
---------------------

.. automodule:: pyscribus.file.colors
    :members:
    :undoc-members:
    :show-inheritance:

pyscribus.file.document
-----------------------

.. automodule:: pyscribus.file.document
    :members:
    :undoc-members:
    :show-inheritance:

pyscribus.file.exceptions
-------------------------

.. automodule:: pyscribus.file.exceptions
    :members:
    :undoc-members:
    :show-inheritance:

pyscribus.file.hyphenation
--------------------------

.. automodule:: pyscribus.file.hyphenation
    :members:
    :undoc-members:
    :show-inheritance:

pyscribus.file.pageobjects
--------------------------

.. automodule:: pyscribus.file.pageobjects
    :members:
    :undoc-members:
    :show-inheritance:

pyscribus.file.stories
----------------------

.. automodule:: pyscribus.file.stories
    :members:
    :undoc-members:
    :show-inheritance:

pyscribus.file.ui
-----------------

.. automodule:: pyscribus.file.ui
    :members:
    :undoc-members:
    :show-inheritance:

