**************************
Frequently asked questions
**************************

File safety
===========

Is PyScribus safe to use in production?
---------------------------------------

No.

Can I overwrite my Scribus files without worry?
-----------------------------------------------

No.

Code
====

Why is ScribusFile not using my unit by default?
------------------------------------------------

``ScribusFile`` can abstract conversions towards pica/DTP points *if* there 
is a unit explicitely defined at ``ScribusFile`` instanciation. Why not simply 
set a default unit to abstract?

1. Someone might want to use pica/DTP points, because they are used to this 
   unit or because it is the unit used in the model – this make switching from 
   **model** to **file** (or from **file** to **model**) easier.
2. It is not the job of PyScribus to guess what unit system you are using. 
   Regional settings are not a good indicator: USA journalists use imperial 
   system while USA scientists, like the rest of the world, use metric system.
3. Setting inches or milimeters as default unit will always be the bad choice 
   for someone and I don't want them to open issues about it. The only sensible 
   way to avoid endless arguments is to annoy everyone equally.
4. You can make a ``ScribusFile`` subclass.

Why is there ``new_*`` methods in ``file``? Is that not pythonic?
-----------------------------------------------------------------

Yes, it isn't.

Some elements of a Scribus file are linked and can't be properly created 
without looking for each other. 

For example, handling a footnote requires:

1. Its **call/mark** in the text frame's story;
2. The **reference to its style** of footnote;
3. The **reference to its call/mark** in the list of all document's marks;
4. The **reference to the text frame** in which the note is displayed (which is not 
   the text frame where the story is);
5. The **text frame** in which the note is displayed (referenced by 4).

So if you want to add a footnote in a paragraph, that footnote must call 2 to 4 
elements – which are spread around the document. 

Unfortunately, using Python context managers would not solve anything. The 
simplest way to get those is to require the paragraph to hold a reference to 
the document. And because paragraphs make no sense without a story, the story 
must also hold a reference to the document…

Distribution / Licensing
========================

Could you host PyScribus on Github?
-----------------------------------

No, it is not ethical to do that.

`Please don’t upload PyScribus on GitHub <https://nogithub.codeberg.page/>`_.

Could you switch PyScribus' licence to MIT?
-------------------------------------------

Absolutely not and I won't change my mind.

This documentation
==================

Why is this documentation written in bad English?
-------------------------------------------------

1. I'm french. Consider the fact that writing an article in English 
   `requires 51% more time <https://theconversation.com/non-native-english-speaking-scientists-work-much-harder-just-to-keep-up-global-research-reveals-208750>`_ 
   for a non-native author, and that after this extra time, copy-editing by 
   someone else remains necessary.
2. Nobody tells me that there are errors in the text, even though it's 
   possible to `point them out <https://framagit.org/etnadji/pyscribus/-/issues>`_ 
   and/or `correct them <https://framagit.org/etnadji/pyscribus/-/merge_requests>`_.

