**************
SLA file model
**************

.. automodule:: pyscribus.model
    :members:
    :undoc-members:
    :show-inheritance:

Basis
=====

pyscribus.model.common.xml
--------------------------

.. automodule:: pyscribus.model.common.xml
    :members:
    :undoc-members:
    :show-inheritance:

pyscribus.model.common.math
---------------------------

.. automodule:: pyscribus.model.common.math
    :members:
    :undoc-members:
    :show-inheritance:

pyscribus.model.exceptions
--------------------------

.. automodule:: pyscribus.model.exceptions
    :members:
    :undoc-members:
    :show-inheritance:

Usual
=====

pyscribus.model.dimensions
--------------------------

.. automodule:: pyscribus.model.dimensions
    :members:
    :undoc-members:
    :show-inheritance:

pyscribus.model.document
------------------------

.. automodule:: pyscribus.model.document
    :members:
    :undoc-members:
    :show-inheritance:

pyscribus.model.layers
----------------------

.. automodule:: pyscribus.model.layers
    :members:
    :undoc-members:
    :show-inheritance:

pyscribus.model.colors
----------------------

.. automodule:: pyscribus.model.colors
    :members:
    :undoc-members:
    :show-inheritance:

pyscribus.model.patterns
------------------------

.. automodule:: pyscribus.model.patterns
    :members:
    :undoc-members:
    :show-inheritance:

pyscribus.model.pageobjects
---------------------------

.. automodule:: pyscribus.model.pageobjects
    :members:
    :undoc-members:
    :show-inheritance:

pyscribus.model.pages
---------------------

.. automodule:: pyscribus.model.pages
    :members:
    :undoc-members:
    :show-inheritance:

pyscribus.model.sla
-------------------

.. automodule:: pyscribus.model.sla
    :members:
    :undoc-members:
    :show-inheritance:

pyscribus.model.stories
-----------------------

.. automodule:: pyscribus.model.stories
    :members:
    :undoc-members:
    :show-inheritance:

pyscribus.model.notes
---------------------

.. automodule:: pyscribus.model.notes
    :members:
    :undoc-members:
    :show-inheritance:

pyscribus.model.marks
---------------------

.. automodule:: pyscribus.model.marks
    :members:
    :undoc-members:
    :show-inheritance:

pyscribus.model.styles
----------------------

.. automodule:: pyscribus.model.styles
    :members:
    :undoc-members:
    :show-inheritance:

pyscribus.model.toc
-------------------

.. automodule:: pyscribus.model.toc
    :members:
    :undoc-members:
    :show-inheritance:

pyscribus.model.printing
------------------------

.. automodule:: pyscribus.model.printing
    :members:
    :undoc-members:
    :show-inheritance:

pyscribus.model.itemattribute
-----------------------------

.. automodule:: pyscribus.model.itemattribute
    :members:
    :undoc-members:
    :show-inheritance:

Headless
========

.. automodule:: pyscribus.model.headless
    :members:
    :undoc-members:
    :show-inheritance:

Script: topdf
-------------

.. automodule:: pyscribus.model.headless.scripts.topdf
    :members:
    :undoc-members:
    :show-inheritance:

Paper sizes
===========

pyscribus.model.papers.iso216
-----------------------------

.. automodule:: pyscribus.model.papers.iso216
    :members:
    :undoc-members:
    :show-inheritance:

pyscribus.model.papers.iso269
-----------------------------

.. automodule:: pyscribus.model.papers.iso269
    :members:
    :undoc-members:
    :show-inheritance:

pyscribus.model.papers.iso217
-----------------------------

.. automodule:: pyscribus.model.papers.iso217
    :members:
    :undoc-members:
    :show-inheritance:

pyscribus.model.papers.afnor
----------------------------

.. automodule:: pyscribus.model.papers.afnor
    :members:
    :undoc-members:
    :show-inheritance:

pyscribus.model.papers.ansi
---------------------------

.. automodule:: pyscribus.model.papers.ansi
    :members:
    :undoc-members:
    :show-inheritance:

