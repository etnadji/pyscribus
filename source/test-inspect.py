#!/usr/bin/python3
# -*- coding:Utf-8 -*-

"""
"""

from pprint import pprint
import pyscribus.file as psf

if __name__ == "__main__":
    scribus_file = psf.ScribusFile("", "tests/images.sla", coding_unit="mm")

    # Just to avoid writing scribus_file.pageobjects each time
    doc_objects = scribus_file.pageobjects

    print("Iterating over page objects:")
    for page_object in doc_objects:
        print(f"\t{page_object.name}:{page_object.ptype}")

    print("")
    print("Reading using items")
    print("\tFirst object is", doc_objects[0])
    pprint(doc_objects.items)

    images = doc_objects.filter(object_type="image")

    print("")
    print("All the image frames of the document:")
    pprint([image.name for image in images])
    print("")

    image_page_two = images[-1]

    print(f"Is image frame “{image_page_two.name}” on page two ? ", end="")
    print(doc_objects.on_page(image_page_two, 1))
    print("")

    print("All the image frames with name matching 'image_*':")
    images_underscored = doc_objects.filter(name="image_*")
    pprint([image.name for image in images_underscored])
    print("")

    polys = doc_objects.filter(object_type="polygon")

    print("All the polygons of the document:")
    pprint([poly.name for poly in polys])
    print("")

    print(
        "Is On_Two_Pages overlaping on page 1 ?",
        doc_objects.overlaps_page(polys[-1], 0)
    )
    print(
        "Is On_Two_Pages overlaping on page 2 ?",
        doc_objects.overlaps_page(polys[-1], 1)
    )
    print(
        "Is On_Two_Pages overlaping on page 3 ?",
        doc_objects.overlaps_page(polys[-1], 2)
    )
    print("")

    polys = doc_objects.filter(object_type="polygon", page_overlap=0)
    print("Polygons on page 1 (non strict):")
    pprint([poly.name for poly in polys])
    print("")

    polys = doc_objects.filter(object_type="polygon", page_overlap_strict=0)
    print("Polygons on page 1 (strict):")
    pprint([poly.name for poly in polys])
    print("")

    print("Vertical guides in the pages")

    for page in scribus_file.pages.pages:
        print(page.number, ":", page.vertical_guides)

    print("\nHorizontal guides in the pages")

    for page in scribus_file.pages.pages:
        print(page.number, ":", page.horizontal_guides)

# vim:set shiftwidth=4 softtabstop=4:
