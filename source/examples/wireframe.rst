Draw a wireframe of a file
--------------------------

  ::

   import pyscribus.file as psf
   import pyscribus.model.extra.wireframe as wire

   slafile = psf.ScribusFile("wireframe.sla", "1.5.5")

   wireframe = wire.Wireframe()
   wireframe.from_sla(slafile.model)

   wireframe.draw(
       output="wireframe.png",
       stylesheet=True,
       margins=[10, 10]
   )

.. figure:: ../_static/wireframe_in.png
   :align: center
   :scale: 25%

   Original SLA file. Cropped on the left side.

.. figure:: ../_static/wireframe_out.png
   :align: center
   :scale: 20%

   Wireframe
