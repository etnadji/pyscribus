**********************
Filtering page objects
**********************

Example file
============

`Download the example file <https://framagit.org/etnadji/pyscribus/-/raw/master/source/examples/page_object_filtering.sla?inline=false>`_.

.. figure:: ../_static/po_filtering_overview.png
   :align: center
   :scale: 50%

   Overview of the example file. Five image frames with five text frames
   labelling them from A to E. A red polygon appear on the top right corner
   of every right page, because it is placed on the right master page. Pages 
   are facing, with A overlaping on page 1 and D overlaping on pages 1 to 3.

Code base
---------

The following lines will be omitted in the examples:

  ::

   import pyscribus.file as psf
   parsed = psf.ScribusFile("1.5.8", "page_object_filtering.sla")

   # Not necessary, just to make next codes shorter
   doc_objects = parsed.pageobjects

Filtering by type
=================

The filter/argument ``object_type`` returns page objects of a certain type.

  ::

   frames = doc_object.filter(object_type="image")

   for page_object in frames:
       print(page_object.name)

Resulting in:

  ::

   text_A
   text_B
   text_C
   text_D
   text_E

Filtering by name
=================

The filter/argument ``name`` returns page objects whose names match an
`Unix glob pattern <https://en.wikipedia.org/wiki/Glob_(programming)>`_.

  ::

   text_frames = doc_object.filter(object_type="text")

   for page_object in text_frames:
       print(page_object.name)

Resulting in:

  ::

   image_A
   image_B
   image_C
   image_D
   image_E

Filtering by presence on pages
==============================

There is 3 filters/arguments related to the presence of a page object on a page:

- ``page``
- ``page_overlap``
- ``page_overlap_strict``

The difference between them has to do with page object's corners.

All of them filter out by default objects in master pages, unless the 
``master`` argument of ``PageObjects.filter`` is set to ``True``.

page
----

``page`` filter returns page objects which have their 4 corners on a page.
In short, this filter returns objects totally contained by a page.

  ::

   # 0 is page 1, 1 is page 2, etc.
   on_first_page = doc_object.filter(page=0, object_type="image")

   for page_object in on_first_page:
       print(page_object.name)

Resulting in:

  ::

   image_B
   image_C

- Images frames B and C are totally contained by the first page.
- Image frame A and D are not listed, because some of their corners are not in 
  the first page (A has two corners on page 1, D has only one, instead of four).
- The red polygon is not listed because it is placed on a master page.
- Image frame E is not on page 1.

page_overlap
------------

``page_overlap`` filters returns page objects having at least 1 corner on a
page. The objects returned may be totally contained by that page, or they may
not.

In short, this filter returns objects totally or partially contained by a page.

  ::

   # 0 is page 1, 1 is page 2, etc.
   on_first_page = doc_object.filter(page_overlap=0, object_type="image")

   for page_object in on_first_page:
       print(page_object.name)

Resulting in:

  ::

   image_A
   image_B
   image_C
   image_D

- Images frames B and C are totally contained by the first page, so each of
  their corners overlap on the page.
- Image frame A and D are listed, because some of their corners are in 
  the first page — two for A and one for D.
- The red polygon is not listed because it is placed on a master page.
- Image frame E is not on page 1.

page_overlap_strict
-------------------

``page_overlap_strict`` filters returns page objects having a maximum of three 
corners on a page.

In short, this filter returns only objects that are partially contained by a page.


  ::

   # 0 is page 1, 1 is page 2, etc.
   on_first_page = doc_object.filter(page_overlap_strict=0, object_type="image")

   for page_object in on_first_page:
       print(page_object.name)

Resulting in:

  ::

   image_A
   image_D

- Images frames B and C are not listed because they are totally contained by
  the first page, 
- Image frame A and D are listed, because some of their corners are in 
  the first page — two for A and one for D — but not all of them.
- The red polygon is not listed because it is placed on a master page.
- Image frame E is not on page 1.

