#!/usr/bin/python3
# -*- coding:Utf-8 -*-

"""
Draw a wireframe representation of a SLA file into "test_wireframe.png".
"""

import pyscribus.file as psf
import pyscribus.model.extra.wireframe as wire

if __name__ == "__main__":
    slafile = psf.ScribusFile("1.5.5", "tests/wireframe.sla")

    wireframe = wire.Wireframe()
    wireframe.from_sla(slafile.model)

    wireframe.draw(
        output="tests-outputs/test_wireframe.png",
        stylesheet=True,
        margins=[10, 10],
    )

# vim:set shiftwidth=4 softtabstop=4:
