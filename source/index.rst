*********
PyScribus
*********

.. image:: https://img.shields.io/pypi/v/pyscribus.svg?label=latest%20version
    :alt: PyPi Latest Version
    :target: https://pypi.python.org/pypi/pyscribus
.. image:: https://www.repostatus.org/badges/latest/wip.svg
    :alt: Project Status: WIP – Initial development is in progress, but there has not yet been a stable, usable release suitable for the public.
    :target: https://www.repostatus.org/#wip
.. image:: https://nogithub.codeberg.page/badge.svg
    :alt: Please don't upload to GitHub
    :target: https://nogithub.codeberg.page

PyScribus aims to read, create and update Scribus .sla files.

.. note::

   As the version number suggests, PyScribus is *not* currently safe to use in
   production. Always make backup copies of your files, don't overwrite
   them, etc.

   I welcome all feedback, constructive suggestions and other contributions.

PyScribus is free software: you can redistribute it and/or modify it under the 
terms of the :ref:`GNU Affero General Public License <agpl_license>`.

.. warning::

   PyScribus 0.3 breaks compatibility with older versions. 

   **Read** the 
   “:ref:`PyScribus low and high level submodules <model_file_organisation>`” 
   section.

**For Python versions >= 3.8**

``pip install pyscribus``

**For older Python versions**

``pip install pyscribus-backported``

-------------------

User Guide
==========

.. toctree::
   :maxdepth: 1

   guide.rst
   guide/en/quickstart.rst
   examples/index.rst

Documentation
=============

.. toctree::
   :maxdepth: 1

   file.rst
   model.rst

SLA specification
=================

.. toctree::
   :maxdepth: 1

   specification.rst

Appendix
========

.. toctree::
   :maxdepth: 1

   faq.rst
   credits.rst
   thanks.rst
   license.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
